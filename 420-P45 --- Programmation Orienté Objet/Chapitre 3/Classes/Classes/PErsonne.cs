﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classes
{
    class Personne
    {
        private int age;
        private string nom;
        
        // Sans qualificatif (public, private ou protected), ce sera privé par défaut
        string prenom;
        DateTime date_de_naissance;

        // Constructeurs
        public Personne()
        {
            nom = "";
            prenom = "";
            date_de_naissance = new DateTime(01, 01, 01);
            calculerAge();
        }

        public void calculerAge()
        {

            long temp = DateTime.Now.Ticks - date_de_naissance.Ticks;
            double age_d = Math.Floor(temp / (365.0 * 10000 * 1000 * 3600 * 24));
            age = (int) age_d;
        }


        public override string ToString()
        {
            return "Je m'appelle " + nom + " et je suis né le " + date_de_naissance.ToLongDateString() + ". J'ai donc " + age + " ans.";
        }

        public void setDateDeNaissance(DateTime d)
        {
            date_de_naissance = d;
            calculerAge();
        }

        // Accesseur publics (read / write)
        public string Nom
        {
            get {
                return nom;
            }
            set
            {
                nom = value;
            }
        }

        // Write only
        public string Prenom
        {
            set{
                prenom = value;
            }
        }
    }

}
