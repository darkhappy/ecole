﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestUnitaires
{
    class Auto
    {
        int _ConsommationRoute;
        int _ConsommationVille;
        int _CapaciteReservoir;
        int _DistanceMaxVille;
        int _DistanceMaxRoute;

        #region Accesseur

        public int CapaciteReservoir
        {
            get
            {
                return _CapaciteReservoir;
            }
            set
            {
                if (value > 0)
                {
                    _CapaciteReservoir = value;

                    if (_ConsommationRoute > 0) 
                        _DistanceMaxRoute = _CapaciteReservoir / _ConsommationRoute;

                    if( _ConsommationVille > 0)
                        _DistanceMaxVille = _CapaciteReservoir / _ConsommationVille;
                }
            }
        }

        public int ConsommationRoute
        {
            get
            {
                return _ConsommationRoute;
            }
            set
            {
                if (value > 0)
                {
                    _ConsommationRoute = value;

                    if (_CapaciteReservoir > 0)
                    {
                        _DistanceMaxRoute = _CapaciteReservoir / _ConsommationRoute;
                    }
                }

            }
        }


        public int ConsommationVille
        {
            get
            {
                return _ConsommationVille;
            }
            set
            {
                if (value > 0)
                {
                    _ConsommationVille = value;

                    if (_CapaciteReservoir > 0)
                        _DistanceMaxVille = _CapaciteReservoir / _ConsommationVille;
                }
            }
        }

        public int DistanceMaxRoute
        {
            get
            {
                return _DistanceMaxRoute;
            }
        }

        public int DistanceMaxVille
        {
            get
            {
                return _DistanceMaxVille;
            }
        }


        #endregion




    }
}
