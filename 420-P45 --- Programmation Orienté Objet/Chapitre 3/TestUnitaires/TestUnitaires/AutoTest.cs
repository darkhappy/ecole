﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace TestUnitaires
{
    [TestFixture]
    class AutoTest
    {
        Auto a;

        [SetUp]
        public void Init()
        {
            a = new Auto();
        }


        [Test]
        public void Auto1a()
        {
            a.ConsommationVille = 10;
            a.CapaciteReservoir = 40;

            Assert.That(a.DistanceMaxVille, Is.EqualTo(400));
        }

        [Test]
        public void Auto1b()
        {
            a.ConsommationVille = 10;
            a.CapaciteReservoir = 40;

            Assert.That(a.DistanceMaxRoute, Is.EqualTo(0));
        }


        [Test]
        public void Auto2a()
        {
            a.CapaciteReservoir = 40;
            a.ConsommationRoute = 5;

            Assert.That(a.DistanceMaxRoute, Is.EqualTo(800));
        }

        [Test]
        public void Auto2b()
        {
            a.CapaciteReservoir = 40;
            a.ConsommationRoute = 5;

            Assert.That(a.DistanceMaxVille, Is.EqualTo(0));
        }

        [Test]
        public void Auto3()
        {

                
        }
    }
}
