﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        DataSet ds;

        public Form1()
        {
            InitializeComponent();

            //InitializeComponent();

            ds = new DataSet();
            DataTable dtProvinces = new DataTable("dtProvinces");
            DataTable dtVilles = new DataTable("dtVilles");

            ds.Tables.Add(dtProvinces);
            ds.Tables.Add(dtVilles);

            DataColumn id = new DataColumn("id", typeof(Int32));
            id.Unique = true;
            id.AutoIncrement = true;
            DataColumn province = new DataColumn("province", typeof(String));
            dtProvinces.Columns.AddRange(new DataColumn[] { id, province });
            dtProvinces.PrimaryKey = new DataColumn[] { id };

            DataColumn id2 = new DataColumn("id", typeof(Int32));
            id2.Unique = true;
            id2.AutoIncrement = true;
            DataColumn fk_province = new DataColumn("fk_province", typeof(Int32));
            DataColumn ville = new DataColumn("ville", typeof(String));
            dtVilles.Columns.AddRange(new DataColumn[] { id2, fk_province, ville });
            dtVilles.PrimaryKey = new DataColumn[] { id2 };

            //Relations
            ds.Relations.Add(new DataRelation("VilleProvince", dtProvinces.Columns["id"], dtVilles.Columns["fk_province"]));


            //Provinces
            DataRow dr = dtProvinces.NewRow();
            dr[1] = "Québec";
            dtProvinces.Rows.Add(dr);
            dr = dtProvinces.NewRow();
            dr[1] = "Ontario";
            dtProvinces.Rows.Add(dr);

            // Villes
            dr = dtVilles.NewRow();
            dr[1] = 0;
            dr["ville"] = "Québec";
            dtVilles.Rows.Add(dr);

            dr = dtVilles.NewRow();
            dr[1] = 0;
            dr["ville"] = "Montréal";
            dtVilles.Rows.Add(dr);

            dr = dtVilles.NewRow();
            dr[1] = 1;
            dr["ville"] = "Toronto";
            dtVilles.Rows.Add(dr);

            dr = dtVilles.NewRow();
            dr[1] = 1;
            dr["ville"] = "Ottawa";
            dtVilles.Rows.Add(dr);

            //cb_Province.DataSourceSource = "{Binding}";
            //cb_Province.DataContext = dtProvinces;
            //cb_Province.DisplayMemberPath = "province";
            //cb_Province.SelectedValuePath = "id";
            cb_Province.DataSource = dtProvinces;
            cb_Province.DisplayMember = "province";
            cb_Province.ValueMember = "id";
        }

        private void cb_Province_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cb_Province.SelectedIndex > -1)
            {
                cb_Ville.DataSource = ds.Tables["dtProvinces"];
                cb_Ville.DisplayMember = "VilleProvince.ville";
                cb_Ville.ValueMember = "VilleProvince.id";
            }
        }

    }
}
