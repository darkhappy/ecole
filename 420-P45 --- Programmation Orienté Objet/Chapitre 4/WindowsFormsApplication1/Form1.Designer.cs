﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cb_Province = new System.Windows.Forms.ComboBox();
            this.cb_Ville = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // cb_Province
            // 
            this.cb_Province.FormattingEnabled = true;
            this.cb_Province.Location = new System.Drawing.Point(93, 53);
            this.cb_Province.Name = "cb_Province";
            this.cb_Province.Size = new System.Drawing.Size(121, 24);
            this.cb_Province.TabIndex = 0;
            this.cb_Province.SelectedIndexChanged += new System.EventHandler(this.cb_Province_SelectedIndexChanged);
            // 
            // cb_Ville
            // 
            this.cb_Ville.FormattingEnabled = true;
            this.cb_Ville.Location = new System.Drawing.Point(93, 121);
            this.cb_Ville.Name = "cb_Ville";
            this.cb_Ville.Size = new System.Drawing.Size(121, 24);
            this.cb_Ville.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(282, 253);
            this.Controls.Add(this.cb_Ville);
            this.Controls.Add(this.cb_Province);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cb_Province;
        private System.Windows.Forms.ComboBox cb_Ville;
    }
}

