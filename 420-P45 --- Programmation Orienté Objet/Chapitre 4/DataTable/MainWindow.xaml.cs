﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        DataSet ds;

        public MainWindow()
        {
            InitializeComponent();

            ds = new DataSet();
            DataTable dtProvinces = new DataTable("dtProvinces");
            DataTable dtVilles = new DataTable("dtVilles");

            ds.Tables.Add(dtProvinces);
            ds.Tables.Add(dtVilles);

            DataColumn id = new DataColumn("id", typeof(Int32));
            id.Unique = true;
            id.AutoIncrement = true;
            DataColumn province = new DataColumn("province", typeof(String));
            dtProvinces.Columns.AddRange(new DataColumn[] { id, province });
            dtProvinces.PrimaryKey = new DataColumn[] { id };
            
            DataColumn id2 = new DataColumn("id", typeof(Int32));
            id2.Unique = true;
            id2.AutoIncrement = true;
            DataColumn fk_province = new DataColumn("fk_province", typeof(Int32));
            DataColumn ville = new DataColumn("ville", typeof(String));
            dtVilles.Columns.AddRange(new DataColumn[] { id2, fk_province, ville });
            dtVilles.PrimaryKey = new DataColumn[] { id2 };

            //Relations
            ds.Relations.Add(new DataRelation("VilleProvince", dtProvinces.Columns["id"], dtVilles.Columns["fk_province"], false));
            
            
            //Provinces
            DataRow dr = dtProvinces.NewRow();
            dr[1] = "Québec";
            dtProvinces.Rows.Add(dr);
            dr = dtProvinces.NewRow();
            dr[1] = "Ontario";
            dtProvinces.Rows.Add(dr);

            // Villes
            dr = dtVilles.NewRow();
            dr[1] = 0;
            dr["ville"] = "Québec";
            dtVilles.Rows.Add(dr);

            dr = dtVilles.NewRow();
            dr[1] = 0;
            dr["ville"] = "Montréal";
            dtVilles.Rows.Add(dr);

            dr = dtVilles.NewRow();
            dr[1] = 1;
            dr["ville"] = "Toronto";
            dtVilles.Rows.Add(dr);

            dr = dtVilles.NewRow();
            dr[1] = 1;
            dr["ville"] = "Ottawa";
            dtVilles.Rows.Add(dr);

            //cb_Province.ItemsSource = "{Binding}";
            //cb_Province.DataContext = dtProvinces;
            this.DataContext = dtProvinces;
            cb_Province.DisplayMemberPath = "province";
            cb_Province.SelectedValuePath = "id";

        }

        private void cb_Province_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //cb_Villes.ItemsSource = "{Binding SelectedItem, ELementName=cb_Province}";
            //cb_Villes.DataContext = ds.Tables["dtProvinces"];

            int selected = (int) cb_Province.SelectedValue;
            DataView dv = ds.Tables["dtVilles"].DefaultView;
            dv.RowFilter = "fk_province=" + selected;

            cb_Villes.DataContext = dv;
            cb_Villes.DisplayMemberPath = "ville";
            cb_Villes.SelectedValuePath = "id";
        }
    }
}
