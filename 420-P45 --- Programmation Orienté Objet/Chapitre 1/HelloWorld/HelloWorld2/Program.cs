﻿//Notez la présence ci-haut du nom du projet.NomDeClasse

//Notez la présence de using ici, ce qui spécifie que tous les namespaces et classes de System pourront être utilisés à l'intérieur de ce programme
using System;

namespace HelloWorld2
{
    class Program
    {
        static void Main(string[] args)
        {

            // Vous voyez l'économie d'espace dans votre programme
            Console.WriteLine("Bonjour");

            // au lieu de 
            System.Console.WriteLine("Bonjour");
        }
    }
}
