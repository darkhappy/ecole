﻿
// NameSpace de l'application créée en Visual Studio
namespace HelloWorld
{
    // Classe générée et appelée lors de l'exécution de l'application ligne de commande
    class Program
    {
        // Chemin d'entrée pour le programme, elle peut recevoir une liste d'arguments comme paramètres
        static void Main(string[] args)
        {

            // Voici un commentaire, veuillez noter la présence du namespace System devant la Classe Console
            // Il est également à noter que Console est en bleu, il s'agit d'une classe
            System.Console.WriteLine("Bonjour les élèves");

            // Pour permettre de mieux voir l'exécution du programme, n'hésitez pas à l'exécuter avec Ctrl-F5
            // ou sinon décommenter la ligne de code suivante.
            // System.Console.ReadLine();
        }
    }
}
