﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;


namespace ProjetXML
{
    class Program
    {
        static void Main(string[] args)
        {
            // Je me crée un reader pour le fichier exemple
            XmlTextReader reader = new XmlTextReader("exemple.xml");
            

            // Je loop et j'affiche les données:
            while (reader.Read())
            {
                if (reader.NodeType != XmlNodeType.Whitespace)
                {
                    Console.WriteLine("Nom du noeud actuel: " + reader.Name);
                    Console.WriteLine("Type du noeud actuel: " + reader.NodeType);
                    Console.WriteLine("Valeur actuelle: " + reader.Value.Trim());
                    for (int i = 0; i < reader.AttributeCount; i++)
                        Console.WriteLine("Attributs {0} : {1}", i, reader.GetAttribute(i));

                    Console.ReadLine();
                }
                
            }

        }
    }
}
