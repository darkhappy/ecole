﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Exceptions
{
    class Program
    {
        static int Division(int a, int b) { return a / b;}

        static void Echange(ref string a, ref string b)
        {
            if (a == null || b == null)
                throw new ArgumentNullException();  

            string temp = a;
            a = b;
            b = temp;
        }


        static void Main(string[] args)
        {
            int a, b, c;

            
            // Sans erreur
            a = 3; b = 5;
            c = Division(a, b);

            // Gestion d'exception
            try
            {
                a = 2; b = 0;
                c = Division(a, b);
            }
            catch (Exception e)
            {
                // Explication de l'exception des messages
                Console.WriteLine(e.Message);
            }

            string g = null;
            string h = "h";

            try
            {
                Echange(ref g, ref h);
            }
            catch (ArgumentNullException e)
            {
                // Nom de l'exception
                Console.WriteLine(e.GetType());
            }


        }
    }
}
