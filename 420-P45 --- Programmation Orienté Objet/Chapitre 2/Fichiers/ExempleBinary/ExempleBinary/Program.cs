﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ExempleBinary
{
    class Program
    {
        static void Main(string[] args)
        {
            FileInfo a = new FileInfo("a.txt");

            using (BinaryWriter bw = new BinaryWriter(a.OpenWrite()))
            {
                double pi = 3.1415;
                int million = 1000000;
                string prof = "Michel Di Croci";

                bw.Write(pi);
                bw.Write(million);
                bw.Write(prof);
            }

            using (BinaryReader br = new BinaryReader(a.OpenRead()))
            {
                Console.WriteLine(br.ReadDouble());
                Console.WriteLine(br.ReadInt32());
                Console.WriteLine(br.ReadString());
            }

        }
    }
}
