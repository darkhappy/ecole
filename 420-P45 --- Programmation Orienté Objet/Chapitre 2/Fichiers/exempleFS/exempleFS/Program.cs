﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace exempleFS
{
    class Program
    {
        static void Main(string[] args)
        {
            using (FileStream fStream = File.Open(@"Message.dat", FileMode.Create))
            {
                string msg = "J'aime vraiment le chocolat! Youppi!";
                string msgEncoded = Convert.ToBase64String(Encoding.UTF8.GetBytes(msg));
                byte[] arrayEncoded = Encoding.Default.GetBytes(msgEncoded);
                fStream.Write(arrayEncoded, 0, arrayEncoded.Length);
                fStream.Position = 0;

                Console.Write("Le message en bytes: ");
                byte[] bytesFromFile = new byte[msgEncoded.Length];
                for (int i = 0; i < bytesFromFile.Length; i++)
                {
                    bytesFromFile[i] = (byte)fStream.ReadByte();
                    Console.Write(bytesFromFile[i]);
                }

                Console.Write("\nDécodage: ");
                string encoded = (Encoding.Default.GetString(bytesFromFile));
                byte[] data = Convert.FromBase64String(encoded);
                string original = Encoding.UTF8.GetString(data);
                Console.WriteLine(original);
            }
        }
    }
}
