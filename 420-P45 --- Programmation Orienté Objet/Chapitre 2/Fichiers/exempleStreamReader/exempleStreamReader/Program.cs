﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace exempleStreamReader
{
    class Program
    {
        static void Main(string[] args)
        {
            using (StreamWriter writer = File.CreateText("rappels.txt"))
            {
                writer.WriteLine("Fête des mères");
                writer.WriteLine("Fête des pères");
                writer.WriteLine("Les numéros suivants:");
                for (int i = 0; i < 10; i++)
                    writer.Write(i + " ");

                // Insertion d'une ligne.
                writer.Write(writer.NewLine);
            }

            using (StreamWriter writer = new StreamWriter("rappels2.txt"))
            {
                writer.WriteLine("Fête des mères");
                writer.WriteLine("Fête des pères");
                writer.WriteLine("Les numéros suivants:");
                for (int i = 0; i < 10; i++)
                    writer.Write(i + " ");

                // Insertion d'une ligne.
                writer.Write(writer.NewLine);
            }

            using (StreamReader sr = new StreamReader("rappels2.txt"))
            {
                while (!sr.EndOfStream)
                {
                    Console.WriteLine(sr.ReadLine());

                }


            }



        }
    }
}
