﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace IO
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] tabFichier = Directory.GetFiles(@"C:\Windows\Web\Wallpaper");

            foreach (string fichier in tabFichier)
                Console.WriteLine(fichier);

            Console.ReadLine();

            DirectoryInfo dir = new DirectoryInfo(@"C:\Windows\Web\Wallpaper");
            FileInfo[] tableauImages = dir.GetFiles("*.jpg");

            foreach (FileInfo f in tableauImages)
            {
                Console.WriteLine("***************************");
                Console.WriteLine("Nom Fichier: {0}", f.Name);
                Console.WriteLine("Taille: {0}", f.Length);
                Console.WriteLine("Création: {0}", f.CreationTime);
                Console.WriteLine("Attributs: {0}", f.Attributes);
                Console.WriteLine("***************************\n");
            }

            DirectoryInfo dir2 = new DirectoryInfo(".");

            // Mon Répertoire
            dir2.CreateSubdirectory(@"Mon Répertoire");

            // Mon Répertoire2
            DirectoryInfo nouveau = dir2.CreateSubdirectory
                (@"Mon Répertoire2\Données");

            DriveInfo[] mesDisques = DriveInfo.GetDrives();

            foreach (DriveInfo d in mesDisques)
            {
                Console.WriteLine("Nom: {0}", d.Name); // C ou D
                Console.WriteLine("Type: {0}", d.DriveType); // fixe

                if (d.IsReady)
                {
                    Console.WriteLine("Espace libre: {0}", d.TotalFreeSpace);
                    Console.WriteLine("Système fichier: {0}", d.DriveFormat);
                    Console.WriteLine("Nom: {0}\n", d.VolumeLabel);
                }
            }

            Console.ReadLine();


        }
    }
}
