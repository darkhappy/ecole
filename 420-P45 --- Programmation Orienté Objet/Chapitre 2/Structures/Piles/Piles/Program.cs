﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace Piles
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack test = new Stack();
            int i = 3;
            float f = 3.5F;
            double d = 3.5348347284;
            decimal m = 39438483434;


            test.Push("Une grosse string");
            test.Push(i);
            test.Push(f);
            test.Push(d);
            test.Push(m);

            Console.WriteLine(test);

            foreach (object o in test)
                Console.WriteLine(o);


            // Voir le dernier élément
            Console.WriteLine(test.Peek());

            // Obtenir le dernier élément et le retirer de la pile
            object a = test.Pop();

            // On sait que le type du dernier est un decimal, on le transtype ici en decimal
            Console.WriteLine(a.GetType());
            decimal a2 = (decimal)a;

            Console.WriteLine(a2);

            while ((i = test.Count)>0)
                test.Pop();

            Console.ReadLine();
            Console.WriteLine(test.Count);
            
            // On peut se sauver d'un Stack empty simplement par l'utilisation d'un try / catch
            // Pas nécessairement une bonne pratique,  veut mieux valider par la propriété Count.
            try { 
                test.Pop();
            }
            catch
            {
                Console.WriteLine("On se sauve");
            }


        }
    }
}
