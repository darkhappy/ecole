﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace ListExample
{
    class Program
    {
        static void Main(string[] args)
        {
            ArrayList l = new ArrayList();
            l.Add("Gateau");
            l.Insert(0, "patate");
            //l.RemoveAt(0);

            Console.WriteLine(l.Contains("Gateau"));

            foreach (object o in l)
                Console.WriteLine(o);

            l.BinarySearch("Gateau");

        }
    }
}
