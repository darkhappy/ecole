﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bool
{
    class Booléen
    {
        static void Main(string[] args)
        {
            // Déclaration
            bool a;

            // Assignation
            a = true;
            a = false;

            // Est-ce que a est égal à false? Notez la différence avec assignation (==)
            Console.WriteLine(a == false);

        }
    }
}
