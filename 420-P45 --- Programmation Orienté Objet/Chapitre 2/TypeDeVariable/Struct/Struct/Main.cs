using System;

namespace Struct
{
	class MainClass
	{
		struct Professeur
		{
			public string nom;
			public string categorie;
			public int age;
			public DateTime date_de_naissance;


			// Ceci est une passe passe que vous verrez davantage dans le prochain cours. Il permet de pouvoir exécuter 
			// un Console.WriteLine directement sur l'objet en cours
			public override string ToString ()
			{
				return nom + " est un professeur d'" + categorie + ", âgé de " + age + " ans, né le " + date_de_naissance.ToLongDateString() + ".";
			}
		}

		public static void Main (string[] args)
		{
			// Création de l'objet de la structure
			Professeur michel;

			// Affectation des données 
			michel.nom = "Michel Di Croci";
			michel.categorie = "informatique";
			michel.age = 33;
			michel.date_de_naissance = new DateTime(1979, 10, 13);

			// Ceci ne fonctionne pas puisqu'il ne sait pas comment afficher la structure directement
			Console.WriteLine (michel);


		}
	}
}
