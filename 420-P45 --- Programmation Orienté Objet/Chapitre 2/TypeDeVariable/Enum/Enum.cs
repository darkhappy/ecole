﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace @enum
{
    class Program
    {
        // Une énumération permet de définir un type que nous pourrons utiliser ultérieurement.
        // Par exemple, les jours de la semaine ou les mois peuvent s'avérer utile:
        enum LesMois
        {
            Janvier, Février, Mars, Avril, Mai, Juin, Juillet, Août, Septembre, Octobre, Novembre,
            Décembre
        }

        static void Main(string[] args)
        {
            // On peut maintenant utiliser le point pour permettre d'affecter les données.

            LesMois MoisDate = LesMois.Janvier;

            Console.WriteLine(LesMois.Janvier);


        }
    }
}
