﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CharString2
{
    class Program
    {
        static void Main(string[] args)
        {
            bool langChoisie = false; 
            do
            {
                Console.WriteLine("Quelle est votre langue? [Fr, En]");
                string langParlee = Console.ReadLine();
                switch (langParlee.ToUpper())
                {
                    case "FR":
                        Console.WriteLine("Vous parlez français très bien");
                        langChoisie = true;
                        break;
                    case "EN":
                        Console.WriteLine("English is a world language");
                        langChoisie = true;
                        break;
                    default:
                        Console.WriteLine("De quessé?");
                        langChoisie = false;
                        break;
                }
            } while (!langChoisie);

        }
    }
}
