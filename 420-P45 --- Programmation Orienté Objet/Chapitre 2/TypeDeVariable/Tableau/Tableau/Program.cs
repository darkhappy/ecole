﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tableau
{
    class Program
    {
        static void Main(string[] args)
        {

            int[] tableauInt = new int[3];
            tableauInt[0] =15;
            tableauInt[1] =10;
            tableauInt[2] =3;

            // Ceci aurait pu être remplacé par l'écriture unique de:
            int[] tableauInt2 = new int[] { 15, 10, 3 };

            foreach(int i in tableauInt)
            {
                Console.WriteLine(i);
            }


        }
    }
}
