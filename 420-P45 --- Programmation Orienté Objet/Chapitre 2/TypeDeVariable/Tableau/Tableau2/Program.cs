﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tableau2
{
    class Program
    {
        static void Main(string[] args)
        {

            object[] ObjTableau=new object[4];
            ObjTableau[0]=false;
            ObjTableau[1]=10;
            ObjTableau[2]="J’aime mon Cégep";
            ObjTableau[3]=new DateTime(1979,10,13);
            foreach(object obj in ObjTableau)
            {
                Console.WriteLine("Type:{0},Value:{1}", obj.GetType(),obj);
            }

            // Chaque tableau créé découle de System.Array donnant une liste de propriétés
            // utiles avec ses objets:Length, Rank, Clear et des méthodes statiques Array.Resize et Array.Find par exemple.
            {
                Console.WriteLine("Longueur du tableau:{0}", ObjTableau.Length);
            }
        }
    }
}
