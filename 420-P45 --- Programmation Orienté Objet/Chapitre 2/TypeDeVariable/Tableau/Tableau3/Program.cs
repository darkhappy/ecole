﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tableau3
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] tableau = new int[2,2];
            tableau[0,0]=3;
            tableau[0,1]=1;
            tableau[1,0]=1;
            tableau[1,1]=3;
            
            for(int i=0;i<2;i++){
                for(int j=0;j<2;j++){
                    Console.Write("tableau[{0},{1}]={2}",i,j,tableau[i,j]);
                }
                Console.WriteLine();
            }

            int[][] tableau2 = new int[3][];
            tableau2[0] = new int[3]{1,35,};
            tableau2[1] = new int[5]{2,4,6,8,10};
            tableau2[2] = new int[1]{0};
            for(int i = 0; i < tableau2.Length;i++){
                for(int j=0; j < tableau2[i].Length;j++){
                    Console.Write("tableau2[{0},{1}]={2}\n",i,j,tableau2[i][j]);
                }
                Console.WriteLine();
            }



        }
    }
}
