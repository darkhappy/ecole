﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Virgules
{
    class Virgule
    {
        static void Main(string[] args)
        {
            // Ce qui détermine la différence entre float et double est la précision nécessaire.
            // Le float est calculé sur 32 bits
            float a = 1.5F; // 7 chiffres de précision

            // Le double est calculé sur 64 bits
            double b = 3.45; // Par défaut, associer à double lorsqu'un réel est pas suffixé
            double c = 3D; // 15-16 chiffres de précision

            // Type encore plus précis que le double sur 128 bits
            decimal d = 3.58M; // 28-29 chiffres de précision



            // Exemple
            int rayon = 3;

            // Ici est un exemple de conversion automatique vers les float
            float aire_f = Convert.ToSingle(Math.PI * Math.Pow((float) rayon, (float) 2));

            // Ici est un exemple de conversion automatique vers les double
            double aire_d = Math.PI * Math.Pow((double)rayon, (double)2);

            decimal aire_m = (decimal) (Math.PI * Math.Pow((double)rayon, (double)2 ));


            Console.WriteLine(aire_f);
            Console.WriteLine(aire_d);

            // Le résultat est calculé sur un double;
            Console.WriteLine(aire_m);

            // Donc une opération entre deux décimaux vous permettra d'obtenir un résultat conséquent
            Console.WriteLine(aire_m / 3.3M);

        }
    }
}
