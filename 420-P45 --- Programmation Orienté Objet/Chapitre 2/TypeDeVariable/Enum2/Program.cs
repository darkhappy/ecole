﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Enum2
{
    class Program
    {
        // Enum d'options d'automobile
        /* FlagsAttribute permet de spécifier que c'est un ensemble d'élément qui peuvent être sélectionnés, pour ce faire.
         * Il est préférable d'utilisser les opérateurs binaires
         */
        [FlagsAttribute]
        enum optionsAuto { 
            lave_glace = 1, 
            fenetre = 2, 
            roues = 4 , 
            pneu = 8, 
            volant = 16, 
            banquette = 32, 
            freins = 64}

        static void Main(string[] args)
        {
            // | est l'opérateur OU en binaire, donc vu que chacun est un flag (on ou off, oui ou non, 1 ou 0)
            // l'option peut donc être activée ou non:
            optionsAuto Saturn = (optionsAuto.fenetre | optionsAuto.volant | optionsAuto.banquette);
            optionsAuto Lada = optionsAuto.volant | optionsAuto.pneu;
            optionsAuto Toyota = (optionsAuto.roues | optionsAuto.lave_glace | optionsAuto.fenetre | optionsAuto.pneu | optionsAuto.volant | optionsAuto.banquette);

            // pour tester la présence d'une option, il faut faire une addition:
            bool freinsToyota = (optionsAuto.freins & Toyota) == optionsAuto.freins ;

            Console.WriteLine("Précense de freins chez Toyota: " + freinsToyota);
            Console.WriteLine(Lada + "\n" + Saturn + "\n" + Toyota );


        }
    }
}
