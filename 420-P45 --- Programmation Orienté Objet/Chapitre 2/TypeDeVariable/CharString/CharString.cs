﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CharString
{
    class CharString
    {
        static void Main(string[] args)
        {
            // Un charactère représente un caractère Unicode
            // Pour plus de détails sur les caractères unicodes:
            // http://fr.wikipedia.org/wiki/Table_des_caract%C3%A8res_Unicode_(0000-0FFF)
            
            // Notez l'apostrophe utilisé pour les caractères
            char lettre = 'a';

            // Notez les guillemets pour les chaînes de caractères
            string chaine = "patate";

            // L'utilisation des crochets permet d'accéder à un élément du tableau, si on figure que la chaîne est un tableau.
            Console.WriteLine(chaine[0]);

            Console.ReadLine();
            string a;
            a="Bonjour";
            Console.WriteLine("Valeur de a: {0}",a);
            Console.WriteLine("a a {0} caractères",a.Length);
            Console.WriteLine("a en caractères majuscules: {0}",a.ToUpper());
            Console.WriteLine("a en caractèresminuscules: {0}",a.ToLower());
            Console.WriteLine("a contient la lettre y: {0}",a.Contains("y"));
            Console.WriteLine("Remplacement d’une lettre de a: {0}", a.Replace("u","ur"));
            Console.WriteLine("Séparation en deux du mots autour d’une lettre: {0}",a.Split('n')[0]);


        }
    }
}
