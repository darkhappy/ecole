﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CharString3
{
    class Program
    {
        static void Main(string[] args)
        {
            StringBuilder sb = new StringBuilder("Ma liste de nom pour mon prochain enfant:");
            sb.Append("\n");
            sb.AppendLine("Matthias");
            sb.AppendLine("Esteban");
            sb.AppendLine("Joël");
            sb.AppendLine("Logan");
            Console.WriteLine(sb);
        }
    }
}
