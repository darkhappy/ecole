﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

// 1) installer le provider de SQLite sur votre ordinateur (si vous n'êtes pas au Cégep)
// Disponible ici: http://sqlite.phxsoftware.com/

// 2) ajouter la référence System.Data.Sqlite
// Changer les propriétés du projet Sqlite pour rendre le projet compatible Framework 3.5 (qui est compatible 2.0)
// Microsoft.CSharp deviendra alors incompatible avec le projet, le supprimer.
using System.Data.SQLite;



namespace Sqlite
{
    class Program
    {
        static void Main(string[] args)
        {
            // On crée nos variables:
            SQLiteConnection connection = new SQLiteConnection();
            
            // Disponible sur le site http://www.connectionstrings.com
            // "Data Source=filename;Version=3;"        
            connection.ConnectionString = "Data Source=test.db;Version=3";
            // Il serait bien sûr possible d'en faire plus, par exemple, mot de passe, mais on va s'en tenir à ça.

            // Ouvre la connexion à la base de donnée
            connection.Open();


            // À présent, il faut se rappeler le SQL que nous avons appris pendant les cours de base de donneés:
            // Créons donc nos tables:

            SQLiteCommand command = new SQLiteCommand("DROP TABLE IF EXISTS Étudiants; CREATE TABLE Étudiants(" +
                " ID integer Primary Key, " +
                " Nom string, " +
                " Prenom string, " +
                " DateDeNaissance date, " +
                " Ville string, " +
                " Technique string, " +
                " NuméroTéléphone string); ");

            command.Connection = connection;
            command.ExecuteNonQuery();

            // Il est possible de faire nos opérations en plus qu'une étape:

            SQLiteCommand command2 = new SQLiteCommand("INSERT INTO Étudiants VALUES ( @ID, @Nom, @Prenom, @DateNaissance, @Ville, @Technique,@No);", connection);
            command2.Parameters.AddWithValue("@ID", null);
            command2.Parameters.AddWithValue("@Nom", "Di Croci");

            SQLiteParameter param3 = new SQLiteParameter("@Prenom");
            param3.Value = "Michel";
            command2.Parameters.Add(param3);

            command2.Parameters.AddWithValue("@DateNaissance", "13/10/1979");
            command2.Parameters.AddWithValue("@Ville", "L'Assomption");
            command2.Parameters.AddWithValue("@Technique", "Informatique");
            command2.Parameters.AddWithValue("@No", "haha!");

            command2.ExecuteNonQuery();

            // Comme vous le constatez, on ne sait pas quel numéro d'enregistrement vient d'être entré...
            // Dans le cas de l'utilisation de clé étrangère (comme dnas notre TP), il peut devenir pratique d'avoir ce numéro
            // dans ce cas, nous devons utiliser select last_insert_rowid

            command.CommandText = "SELECT last_insert_rowid() FROM Étudiants";
            // Comme vous le savez, SELECT nous retourne un élément contrairement à un INSERT ou un UPDATE OU UN DELETE
            // Cela siginifie entre autre que la requête précédente va nous retourner qu'un seul scalaire, sinon nous aurions
            // utilisé le SQLiteDataReader que nous verrons ultérieurement
            object id = command.ExecuteScalar();

            Console.WriteLine(id);

            connection.Close();
        }
    }
}
