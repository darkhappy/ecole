﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TypeDeVariable
{
    class Integer
    {
        static void Main(string[] args)
        {
            // Les types de bases sont les suivants:
            // System.Int16, System.Int32, System.Int64, ..
            
            // Des alias ont été créés pour facilier leur utilisation:
            // short, int, long

            // Signés: -2^31 -1 à 2^31
            int x, y;

            // Non signés : 0 à 2^32
            uint a, b;

            x = 3;
            y = 8;

            a = 3;
            b = 8;

            // Représentation d'un saut de ligne avec le \n 
            Console.Write(x+y + "\n");

            // Représentation d'un autre saut de ligne
            Console.Write(x + y + Environment.NewLine);

            // Les int signés supportent les nombres négatifs:
            Console.WriteLine(x - y);

            // Les dépassement de capacités peuvent être détectés par le code.
            // Une façon lorsque l'option est activée pour ne pas valider, utiliser ceci:
            unchecked
            {
                Console.WriteLine(a - b);
            }

        }
    }
}
