import React from 'react'
// import { StyleSheet, Text, View } from 'react-native'
import { NativeBaseProvider, Text, Box } from 'native-base';
import ProductList from './ProductList'

export default function App() {
  return (
    <NativeBaseProvider>
      <Box flex={1} bg="#fff" alignItems="center" justifyContent="center">
        <Text>Open up App.js to start working on your app!</Text>
      </Box>
    </NativeBaseProvider>
  )
}