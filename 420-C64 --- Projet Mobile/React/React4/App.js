/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState} from 'react';
import { NativeBaseProvider, Center, Text, Button, Box} from 'native-base';

export default App => {
  const [count,setCount] = useState(0)
  return (
    <NativeBaseProvider>
    <Center>
      <Box
        bg="secondary.800"
        p="15"
        mb="20"
        mt="20"
        _text={{
          fontSize: "md",
          fontWeight: "bold",
          color: "white",
        }}>
      Boîte
      </Box>
      <Button bg="primary.800" onPress={()=>setCount(count+1)} ><Text>{count}</Text></Button>
    </Center>
    </NativeBaseProvider>
  )
}