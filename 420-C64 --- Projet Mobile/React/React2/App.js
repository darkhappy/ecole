import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import ProductList from './ProductList'

export default function App() {
  return (
    <View style={styles.container}>
      <Text style={styles.textStyle}>
        Learn React Native avec Michel!
      </Text>
      <ProductList/>      
    </View>
  )
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor: "#eee",
    alignItems: 'center',
    justifyContent: 'center'
  },
  textStyle:{
    fontSize: 50
  }
})