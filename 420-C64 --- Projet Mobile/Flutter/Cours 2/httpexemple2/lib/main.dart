import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'models/user.dart';
import 'dart:convert';

void main() {
  runApp(const MyAppLess());
}

class MyAppLess extends StatelessWidget {
  const MyAppLess({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: const MyAppFull(title: 'JSON send data Demo Page'),
    );
  }
}

class MyAppFull extends StatefulWidget {
  const MyAppFull({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyAppFull> createState() => _MyAppState();
}

class _MyAppState extends State<MyAppFull> {
  late Future<user>? _user;
  String? gender;

  @override
  void initState() {
    super.initState();
    _user = null;
    gender = null;
  }

  void _onPressed() {
    String englishGender = gender == "Homme" ? "male" : "female";
    //print(englishGender);
    setState(() => {_user = fetchUser(englishGender)});
    // getInfo from Interface()
  }

  Future<user> fetchUser(String gender) async {
    final response = await http
        .get(Uri.parse('https://randomuser.me/api/?gender=' + gender));

    if (response.statusCode == 200) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      final json = response.body;
      final extractedData = jsonDecode(json);
      List users = extractedData["results"];
      return user.fromJson(users[0]);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load user');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(children: [
          DropdownButton<String>(
            value: gender,
            icon: const Icon(Icons.arrow_downward),
            elevation: 16,
            style: const TextStyle(color: Colors.deepPurple),
            underline: Container(
              height: 2,
              color: Colors.deepPurpleAccent,
            ),
            onChanged: (String? newValue) {
              setState(() {
                gender = newValue!;
              });
            },
            items: <String>['Homme', 'Femme']
                .map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
          ),
          TextButton(
              child: const Icon(
                Icons.person_search,
              ),
              onPressed: _onPressed),
          Card(
            shape: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: const BorderSide(color: Colors.white24),
            ),
            child: FutureBuilder<user>(
              future: _user,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Image.network(snapshot.data!.pictures.large),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(snapshot.data!.yourname.toString(),
                                style:
                                    const TextStyle(height: 5, fontSize: 20)),
                            Text(snapshot.data!.email),
                            Text(snapshot.data!.cell)
                          ],
                        ),
                      ]);
                } else {
                  return const Text("En cours d'exécution");
                }
              },
            ),
          ),
        ]),
      ),
    );
  }
}
