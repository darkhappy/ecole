import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

void main() {
  runApp(const MyAppLess());
}

class MyAppLess extends StatelessWidget {
  const MyAppLess({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: const MyAppFull(title: 'JSON send data Demo Page'),
    );
  }
}

class MyAppFull extends StatefulWidget {
  const MyAppFull({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyAppFull> createState() => _MyAppState();
}

class _MyAppState extends State<MyAppFull> {
  late Future<String>? _response;

  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _valueController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _response = null;
  }

  void _onPressed() {
    setState(() => {_response = sendInfo()});
    // getInfo from Interface()
  }

  Future<String> sendInfo() async {
    String _body = json
        .encode(<String, String>{_nameController.text: _valueController.text});

    final response = await http.post(
        Uri.parse("https://ptsv2.com/t/znsh7-1644843403/post"),
        headers: <String, String>{'Content-Type': 'application/json'},
        body: _body);

    if (response.statusCode == 200) {
      return "ok";
    } else {
      throw Exception('Failed to connect to toilet');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(children: [
          TextField(
            decoration: const InputDecoration(
              hintText: 'Le name',
            ),
            controller: _nameController,
          ),
          TextField(
            decoration: const InputDecoration(
              hintText: 'Le contenu',
            ),
            controller: _valueController,
          ),
          TextButton(
              child: const Icon(
                Icons.send,
              ),
              onPressed: _onPressed),
          Card(
            shape: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: const BorderSide(color: Colors.white24),
            ),
            child: FutureBuilder<String>(
              future: _response,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return Text(
                    snapshot.data!,
                    style: const TextStyle(height: 5, fontSize: 20),
                  );
                } else {
                  return const Text("En cours d'exécution");
                }
              },
            ),
          ),
        ]),
      ),
    );
  }
}
