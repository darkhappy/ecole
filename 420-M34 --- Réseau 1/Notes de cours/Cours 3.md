# Notions de base sur les communications
La taille, la forme et la fonction des réseaux peuvent varier. Ils peuvent être aussi complexes que des appareils connectés à l'internet, ou aussi simples que deux ordinateurs directement reliés entre eux par un seul câble, et tout ce qui se trouve entre les deux. Cependant, il ne suffit pas de connecter physiquement, via une connexion filaire ou sans fil, des périphériques finaux pour permettre la communication. Les périphériques doivent également savoir comment **communiquer**.

Pour échanger des idées, les personnes utilisent de nombreuses méthodes de communication différentes. Cependant, toutes les méthodes de communication ont en commun trois éléments:

* Source du message (expéditeur) - Les sources du message sont les personnes, ou les appareils électroniques, qui ont besoin d'envoyer un message à d'autres personnes ou appareils.
* Destination du message (destinataire) - La destination reçoit le message et l'interprète.
* Canal - Il s'agit du support qui assure le cheminement du message de sa source à sa destination.

# Protocoles de communication
L'envoi d'un message, que ce soit par une communication en face à face ou sur un réseau, est régi par des règles appelées protocoles. Ces protocoles sont spécifiques au type de méthode de communication utilisé. Dans nos communications personnelles quotidiennes, les règles que nous utilisons pour communiquer à travers un support (par exemple, un appel téléphonique) ne sont pas nécessairement identiques au protocole d'utilisation d'un autre support tel que l'envoi d'une lettre.

Le processus d'envoi d'une lettre est similaire à la communication qui se produit dans les réseaux informatiques.

Voir 3.1.3

# Exigences Relatives au Protocole de Réseau
Les protocoles utilisés dans le cadre des communications réseau partagent bon nombre de ces caractéristiques fondamentales. En plus d'identifier la source et la destination, les protocoles informatiques et réseau définissent la manière dont un message est transmis sur un réseau. Les protocoles informatiques communs comprennent les exigences suivantes :

## Codage des messages 

Pour envoyer un message, il faut tout d'abord le coder. Le codage est le processus de conversion des informations vers un autre format acceptable, à des fins de transmission. Le décodage inverse ce processus pour interpréter l'information. (3.1.6)

## Format et encapsulation des messages

Lorsqu'un message est envoyé de la source à la destination, il doit suivre un format ou une structure spécifique. Les formats des messages dépendent du type de message et du type de canal utilisés pour remettre le message. (3.1.7)

## La taille du message

Les règles qui régissent la taille des parties ou « trames » transmises au réseau sont très strictes. Elles peuvent également être différentes selon le canal utilisé. Les trames trop longues ou trop courtes ne sont pas livrées. 

Les restrictions en termes de taille des trames requièrent de l'hôte source qu'il décompose les longs messages en parties répondant aux impératifs de taille minimale et maximale. Un message long est envoyé en plusieurs trames contenant chacune un fragment du message d'origine. Chaque trame possède également ses propres informations d'adressage. Au niveau de l'hôte destinataire, les différents morceaux du message sont reconstruits de manière à recomposer le message d'origine. (3.1.8)

## Synchronisation des messages

La synchronisation des messages est également très importante dans les communications réseau. 

* Contrôle de flux - Ceci est le processus de gestion de la vitesse de transmission des données. Le contrôle de flux définit la quantité d'informations qui peuvent être envoyées et la vitesse à laquelle elles peuvent être livrées. Si une personne parle trop rapidement, l’autre personne éprouve des difficultés à entendre et à comprendre le message. Dans la communication réseau, il existe des protocoles réseau utilisés par les périphériques source et de destination pour négocier et gérer le flux d'informations.

* Délai de réponse - Si une personne pose une question et qu’elle n’entend pas de réponse dans un délai acceptable, elle suppose qu’aucune réponse n’a été donnée et réagit en conséquence. La personne peut répéter la question ou continuer à converser. Les hôtes du réseau sont également soumis à des règles qui spécifient le délai d'attente des réponses et l'action à entreprendre en cas de délai d'attente dépassé.

* La méthode d'accès - détermine le moment où un individu peut envoyer un message. Cliquez sur Lecture dans la figure pour voir une animation de deux personnes parlant en même temps, puis une "collision d'informations" se produit, et il est nécessaire que les deux personnes s'arrêtent et recommencent. De même, lorsqu'un périphérique souhaite transmettre sur un réseau local sans fil, il est nécessaire que la carte d'interface réseau WLAN (NIC) détermine si le support sans fil est disponible. (3.1.9)

## Mode de transmission des messages

Un message peut être transmis de différentes manières:

* Monodiffusion - Les informations sont transmises à un périphérique terminal unique.(par exemple: message pour Louis)
* Multidiffusion - Les informations sont transmises à un ou plusieurs périphériques finaux. (par exemple: message destiné uniquement aux filles de la classe)
* Diffusion - L'information est transmise à tous les appareils finaux. (par exemple: message destiné à toute la classe)

(3.1.10 et 3.1.11)

# Suites de protocoles réseau

Une suite de protocoles est un groupe de protocoles interdépendants nécessaires pour assurer une fonction de communication.

Pour mieux visualiser l'interaction des protocoles d'une suite, imaginez que celle-ci est une pile. Une pile de protocoles indique comment chacun des protocoles de la suite est mis en oeuvre. Les protocoles sont considérés en termes de couches, chaque service de niveau supérieur dépendant de la fonctionnalité définie par les protocoles indiqués dans les niveaux inférieurs. Les couches inférieures de la pile s'occupent du déplacement de données sur le réseau et de la fourniture de services aux couches supérieures, qui elles, se concentrent sur le contenu du message en cours d'envoi.

Les protocoles TCP/IP sont disponibles pour les couches application, transport et internet. Il n'y a pas de protocole TCP/IP dans la couche d'accès réseau. Les protocoles LAN de couche d'accès réseau les plus courants sont Ethernet et WLAN (LAN sans fil). Les protocoles de la couche d'accès réseau sont responsables de la remise du paquet IP sur le support physique.

![](images/TCPIP1.png)

Aujourd'hui, la suite de protocoles TCP/IP inclut de nombreux protocoles et continue d'évoluer pour prendre en charge de nouveaux services. Les plus courants sont répertoriés dans la figure.

![](images/TCPIP2.png)

## Processus de communication TCP/IP

**Encapsulation**

Prendre les données des étages supérieures et les encapsuler à nos données avant de les transférer finalement sur la couche physique.

Voir animation du 3.3.5

**Désencapsulation**

prendre les données reçues par la couche physique et les désencapsuler étage par étage pour finalement voir les données transmises.

Voir animation du 3.3.6

# Organismes de normalisation
 
## Normes ouvertes

Les normes sont élaborées par des organismes internationaux de normalisation. Les normes ouvertes favorisent l'interopérabilité, la concurrence et l'innovation. Ils garantissent également que le produit d'une seule entreprise ne peut monopoliser le marché ou avoir un avantage déloyal sur sa concurrence.

Pour illustrer ceci, prenons l'exemple de l'achat d'un routeur sans fil par un particulier. De nombreux choix sont disponibles auprès de différents fournisseurs, qui intègrent tous des protocoles standard tels que IPv4, IPv6, DHCP, SLAAC, Ethernet et 802.11 Wireless LAN. Ces normes ouvertes permettent également à un client utilisant le système d'exploitation Apple macOS de télécharger une page web à partir d'un serveur web utilisant le système d'exploitation Linux. Cela s'explique par le fait que les deux systèmes d'exploitation mettent en oeuvre les mêmes protocoles de norme ouverte, notamment ceux de la suite de protocoles TCP/IP.

Les organismes de normalisation sont généralement des associations à but non lucratif qui ne sont liées à aucun constructeur. Leur objectif est de développer et de promouvoir le concept des normes ouvertes. Ces organisations sont importantes pour maintenir un internet ouvert avec des spécifications et des protocoles librement accessibles qui peuvent être mis en œuvre par n'importe quel fournisseur.

Un organisme de normalisation peut rédiger un ensemble de règles entièrement seul ou, dans d'autres cas, peut choisir un protocole propriétaire comme base de la norme. Si un protocole propriétaire est utilisé, il implique généralement le constructeur à l'origine de sa création.

# Modèles de référence

## Avantage de l'utilisation d'un modèle en couches

Vous ne pouvez pas réellement regarder de vrais paquets circuler à travers un vrai réseau, la façon dont vous pouvez regarder les composants d'une voiture assemblés sur une ligne d'assemblage. Donc, cela aide à avoir une façon de penser à un réseau afin que vous puissiez imaginer ce qui se passe. Un modèle est utile dans ces situations.

Des concepts complexes comme le fonctionnement d'un réseau peuvent être difficiles à expliquer et à comprendre. Pour cette raison, un modèle en couches est utilisé pour moduler les opérations d'un réseau en couches gérables.

Ce sont les avantages de l'utilisation d'un modèle en couches pour décrire les protocoles et les opérations du réseau :

* Aide à la conception de protocoles car les protocoles qui fonctionnent à une couche spécifique ont des informations définies sur lesquelles ils agissent et une interface définie avec les couches supérieures et inférieures
* Favoriser la compétition car les produits de différents fournisseurs peuvent fonctionner ensemble
* Empêcher que des changements de technologie ou de capacité dans une couche n'affectent d'autres couches au-dessus et au-dessous
* Fournir un langage commun pour décrire les fonctions et les capacités de mise en réseau
Comme le montre la figure, il existe deux modèles en couches qui sont utilisés pour décrire les opérations réseau :

Modèle de référence pour l'interconnexion des systèmes ouverts (OSI)

![](images/OSI.png)

| Couche du modèle OSI | Description |
| -- | -- |
|7 Application | La couche application contient les protocoles utilisés pour les processus communications.|
|6 Présentation	| La couche de présentation permet une représentation commune des données transférés entre les services de couche d'application.|
|5 Session	| La couche de session fournit des services à la couche de présentation pour organiser son dialogue et gérer l'échange de données.
|4  Transport |	La couche transport définit les services à segmenter, à transférer et réassembler les données pour les communications individuelles entre les terminaux.
| 3 Réseau |	La couche réseau fournit des services permettant d'échanger les différents éléments de données individuels sur le réseau entre des dispositifs terminaux identifiés. |
| 2 Liaison de données | Les protocoles de la couche liaison de données décrivent les méthodes d'échange de de trames de données entre les appareils sur un support commun.| 
|1  Physique |Les protocoles de la couche physique décrivent les moyens mécaniques, électriques, fonctionnels et procéduraux pour activer, maintenir et désactiver des connexions physiques pour la transmission d'un bit vers et depuis un réseau device.|
| |

Note: Alors que les couches du modèle TCP/IP ne sont désignées que par leur nom, les sept couches du modèle OSI sont plus souvent désignées par un numéro plutôt que par leur nom. Par exemple, la couche physique est appelée couche 1 du modèle OSI, la couche de liaison de données est la couche 2, et ainsi de suite.

## Comparaison des modèles OSI et TCP/IP

Les protocoles qui constituent la suite de protocoles TCP/IP peuvent être décrits selon les termes du modèle de référence OSI. Dans le modèle OSI, la couche d'accès réseau et la couche application du modèle TCP/IP sont subdivisées pour décrire les fonctions distinctes qui doivent intervenir sur ces couches.

* Au niveau de la couche d'accès au réseau, la suite de protocoles TCP/IP ne spécifie pas quels protocoles utiliser lors de la transmission à travers un support physique ; elle décrit uniquement la remise depuis la couche internet aux protocoles réseau physiques. 
* Les couches OSI 1 et 2 traitent des procédures nécessaires à l'accès aux supports et des moyens physiques pour envoyer des données sur un réseau.
* La couche application TCP/IP inclut plusieurs protocoles qui fournissent des fonctionnalités spécifiques à plusieurs applications d'utilisateur final. Les couches 5,6 et 7 du modèle OSI servent de références aux développeurs et aux éditeurs de logiciels d'application pour créer des applications qui fonctionnent sur les réseaux.

# Encapsulation des données

En théorie, une seule communication, telle qu'une vidéo ou un message électronique avec de nombreuses pièces jointes volumineuses, pourrait être envoyée à travers un réseau d'une source à une destination sous la forme d'un flux de bits massif et ininterrompu. 

Cependant, cela créerait des problèmes pour d'autres appareils ayant besoin d'utiliser les mêmes canaux ou liens de communication. Ces flux de données volumineux entraîneraient des retards conséquents. En outre, si un lien quelconque de l'infrastructure du réseau interconnecté tombait en panne pendant la transmission, le message complet serait perdu et devrait être retransmis dans son intégralité.

Il existe une meilleure approche, qui consiste à diviser les données en parties de taille moins importante et plus facilement gérables pour les envoyer sur le réseau. La segmentation est le processus consistant à diviser un flux de données en unités plus petites pour les transmissions sur le réseau. La segmentation est nécessaire car les réseaux de données utilisent la suite de protocoles TCP/IP pour envoyer des données dans des paquets IP individuels. Chaque paquet est envoyé séparément, semblable à l'envoi d'une longue lettre sous la forme d'une série de cartes postales individuelles. Les paquets contenant des segments pour la même destination peuvent être envoyés sur des chemins différents.

La segmentation des messages présente deux avantages majeurs :

* Augmente la vitesse - Étant donné qu'un flux de données volumineux est segmenté en paquets, de grandes quantités de données peuvent être envoyées sur le réseau sans attacher une liaison de communication. Cela permet à de nombreuses conversations différentes d'être entrelacées sur le réseau appelé multiplexage.
  
* Augmente l'efficacité - Si un segment ne parvient pas à atteindre sa destination en raison d'une défaillance du réseau ou d'une congestion réseau, seul ce segment doit être retransmis au lieu de renvoyer l'intégralité du flux de données.

(3.6.1)

## Séquençage
La difficulté que présente l'utilisation de la segmentation et du multiplexage pour la transmission des messages à travers un réseau réside dans le niveau de complexité ajouté au processus. Imaginez si vous deviez envoyer une lettre de 100 pages, mais chaque enveloppe ne pouvait contenir qu'une page . Par conséquent, 100 enveloppes seraient nécessaires et chaque enveloppe devrait être traitée individuellement. Il est possible que la lettre de 100 pages dans 100 enveloppes différentes arrive en désordre. Par conséquent, l'information contenue dans l'enveloppe devrait inclure un numéro de séquence pour s'assurer que le destinataire puisse remonter les pages dans l'ordre approprié.

Dans les communications en réseau, chaque segment du message doit passer par un processus similaire pour s'assurer qu'il arrive à la bonne destination et peut être réassemblé dans le contenu du message original, comme le montre la figure. TCP est responsable du séquençage des segments individuels.

La figure montre deux ordinateurs qui envoient des messages sur un réseau à un serveur. Chaque message a été divisé en plusieurs pièces illustrées sous forme d'enveloppes jaunes et orange, certaines sont entrelacées et numérotées. Plusieurs parties sont étiquetées pour être aisément dirigées et réassemblées. L'étiquetage permet de classer et d'assembler les parties lorsqu'elles arrivent.

![](images/S%C3%A9quencage.png)

## Unités de données de protocole

Lorsque les données d'application descendent la pile de protocoles en vue de leur transmission sur le support réseau, différentes informations de protocole sont ajoutées à chaque niveau. Il s'agit du processus d'encapsulation.

Note: Bien que la PDU UDP soit appelée datagramme, les paquets IP sont parfois également appelés datagrammes IP.

La forme qu'emprunte une donnée sur n'importe quelle couche est appelée unité de données de protocole. Au cours de l'encapsulation, chaque couche, l'une après l'autre, encapsule l'unité de données de protocole qu'elle reçoit de la couche supérieure en respectant le protocole en cours d'utilisation. À chaque étape du processus, une unité de données de protocole possède un nom différent qui reflète ses nouvelles fonctions. Bien qu'il n'existe pas de convention de dénomination universelle pour les PDU, dans ce cours, les PDU sont nommés selon les protocoles de la suite TCP/IP. 

![](images/pdu.png)

* Données: terme générique pour l'unité de données de protocole utilisée à la couche application

* Segment: unité de données de protocole pour la couche transport

* Paquet: unité de données de protocole pour la couche réseau

* Trame: unité de données de protocole pour la couche liaison de données

* Bits: PDU utilisée lors de la transmission physique de données sur le support

# Accès aux données

Les couches réseau et liaison de données sont chargées de transmettre les données du périphérique source au périphérique de destination. Comme le montre la figure, les protocoles des deux couches contiennent une adresse source et une adresse destination, mais leurs adresses ont des objectifs différents :

* Adresses source et destination de la couche réseau: Responsable de la livraison du paquet IP de la source d'origine à la destination finale, qui peut être sur le même réseau ou sur un réseau distant.

* Adresses source et destination de la couche de liaison de données: Responsable de la transmission de la trame de liaison de données d'une carte d'interface réseau (NIC) à une autre NIC sur le même réseau.

![](images/Adresses.png)

Une adresse IP est l'adresse logique de la couche réseau, ou couche 3, utilisée pour acheminer le paquet IP de la source d'origine à la destination finale.

Le paquet IP contient deux adresses IP :

* Adresse IP source - L'adresse IP de l'appareil d'envoi, qui est la source originale du paquet.
* Adresse IP de destination - L'adresse IP de l'appareil récepteur, qui est la destination finale du paquet.

Les adresses de couche réseau ou adresses IP indiquent la source d'origine et la destination finale. Cela est vrai que la source et la destination se trouvent sur le même réseau IP ou sur des réseaux IP différents.

![](images/IP.png)

Une adresse IP contient deux parties :

### Partie réseau (IPv4) ou préfixe (IPv6) 

La partie située à l'extrême gauche de l'adresse qui indique le réseau dont l'adresse IP est membre. Tous les périphériques du même réseau ont, dans leur adresse IP, la même partie réseau.

### Partie hôte (IPv4) ou ID d'interface (IPv6) 

La partie restante de l'adresse qui identifie un appareil spécifique sur le réseau. Cette partie est unique pour chaque appareil ou interface sur le réseau.

Note: Le masque de sous-réseau (IPv4) ou préfixe-longueur (IPv6) est utilisé pour identifier la partie réseau d'une adresse IP à partir de la partie hôte.

# Périphériques sur le même réseau

Dans cet exemple, nous avons un ordinateur client (PC1) communiquant avec un serveur FTP sur le même réseau IP.

* Adresse IPv4 source - L'adresse IPv4 de l'appareil émetteur, l'ordinateur client PC1 : 192.168.1.110

* Adresse IPv4 de destination - L'adresse IPv4 de l'appareil récepteur, serveur FTP : 192.168.1.9.

![](images/TransfertEthernet.png)

Remarquez dans la figure que la partie réseau de l'adresse IPv4 source et de l'adresse IPv4 destination se trouve sur le même réseau. 

Remarquez dans la figure que la partie réseau de l'adresse IPv4 source et la partie réseau de l'adresse IPv4 destination sont les mêmes et donc ; la source et la destination sont sur le même réseau.

Adresse IPv4 de destination - L'adresse IPv4 de l'appareil récepteur, serveur FTP : 192.168.1.9.

# Rôle des adresses des couches de liaison de données : Même réseau IP

Lorsque l'expéditeur et le récepteur du paquet IP se trouvent sur le même réseau, la trame de liaison de données est envoyée directement au périphérique récepteur. 

Sur un réseau Ethernet, les adresses de liaison de données sont connues sous le nom d'adresses MAC (Ethernet Media Access Control).

Les adresses MAC sont physiquement incorporées dans la carte réseau Ethernet.

* Adresse MAC source: Il s'agit de l'adresse de liaison de données, ou adresse MAC Ethernet, de l'appareil qui envoie la trame de liaison de données avec le paquet IP encapsulé. L'adresse MAC de la carte réseau Ethernet de PC1 est AA-AA-AA-AA-AA-AA, écrite en notation hexadécimale.

* Destination MAC address: Quand le destinataire est dans le même réseau que l'émetteur, nous utilisons alors l'adresse MAC pour le identifier le destinataire. Dans cet exemple, c'est l'appareil CC-CC-CC-CC-CC-CC, écrit en notation hexadécimale.

La trame contenant le paquet IP encapsulé peut maintenant être transmise par PC1 directement au serveur FTP.

# Périphériques sur un réseau distant

Mais quels sont les rôles de l'adresse de couche réseau et de l'adresse de couche liaison de données lorsqu'un périphérique communique avec un autre périphérique situé sur un réseau distant ?

## Rôle des adresses de la couche réseau

Lorsque l'expéditeur du paquet appartient à un réseau différent de celui du récepteur, les adresses IP source et de destination représentent des hôtes sur différents réseaux. Cette information est indiquée par la partie réseau de l'adresse IP de l'hôte de destination.

* Adresse IPv4 source: L'adresse IPv4 de l'appareil émetteur, l'ordinateur client PC1 : 192.168.1.110

* Adresse IPv4 de destination: L'adresse IPv4 de l'appareil récepteur, serveur Web : 172.16.1.99.

Remarquez dans la figure que la partie réseau de l'adresse IPv4 source et l'adresse IPv4 destination se trouvent sur des réseaux différents.

## Rôle des adresses des couches de liaison de données : Différent réseau IP

Lorsque l'expéditeur et le récepteur du paquet IP se trouvent sur des réseaux différents, la trame liaison de données Ethernet ne peut pas être envoyée directement à l'hôte de destination, car celui-ci n'est pas directement accessible sur le réseau de l'expéditeur. 

Donc, la trame Ethernet doit être envoyée à un autre périphérique appelé routeur ou passerelle par défaut. Dans notre exemple, la passerelle par défaut est R1. R1 dispose d'une adresse de liaison de données Ethernet qui se trouve sur le même réseau que PC1. Cela permet à PC1 d'accéder directement au routeur.

![](images/Passerelle.png)

* Adresse MAC source - L'adresse MAC Ethernet de l'appareil émetteur, PC1. L'adresse MAC de l'interface Ethernet de PC1 est AA-AA-AA-AA-AA-AA.

* Adresse MAC de destination - Lorsque le dispositif récepteur, l'adresse IP de destination, se trouve sur un réseau différent de celui du dispositif émetteur, ce dernier utilise l'adresse MAC Ethernet de la passerelle ou du routeur par défaut. Dans cet exemple, l'adresse MAC de destination est l'adresse MAC de l'interface Ethernet de R1, 11-11-11-11-11-11. Il s'agit de l'interface qui est connectée au même réseau que PC1.

La trame Ethernet contenant le paquet IP encapsulé peut être transmise à R1. R1 achemine le paquet vers la destination, le serveur web. R1 peut transmettre le paquet à un autre routeur ou bien directement au serveur web si la destination se trouve sur un réseau connecté à R1.

Il est important que l'adresse IP de la passerelle par défaut soit configurée sur chaque hôte du réseau local. Tous les paquets dont la destination se trouve sur des réseaux distants sont envoyés à la passerelle par défaut. Les adresses MAC Ethernet et la passerelle par défaut sont examinées plus en détail dans d'autres modules.

# Adresses de liaison de données
L'adresse physique de la couche 2 de la liaison de données a un rôle différent. L'objectif de l'adresse de liaison de données est de transmettre la trame liaison de données d'une interface réseau à une autre, sur un même réseau.

Avant qu'un paquet IP puisse être envoyé sur un réseau câblé ou sans fil, il doit être encapsulé dans une trame de liaison de données, afin qu'il puisse être transmis sur le support physique.

À mesure que le paquet IP voyage d'hôte à routeur, de routeur à routeur, et enfin de routeur à hôte, à chaque point du trajet, le paquet IP est encapsulé dans une nouvelle trame de liaison de données. Chaque trame liaison de données contient l'adresse liaison de données source de la carte réseau qui envoie la trame, et l'adresse liaison de données de destination de la carte réseau qui la reçoit.

La couche 2, le protocole de liaison de données, sert uniquement à remettre le paquet entre les cartes réseau d'un même réseau. Le routeur supprime les informations de couche 2 dès leur réception sur une carte réseau et ajoute de nouvelles informations de liaison de données avant de les transférer vers la destination finale.

Le paquet IP est encapsulé dans une trame de liaison de données qui contient les informations de liaison de données suivantes :

* Adresse de la liaison de données source - L'adresse physique du NIC qui envoie la trame de liaison de données.
* Adresse de liaison de données de destination - L'adresse physique du NIC qui reçoit la trame de liaison de données. Cette adresse correspond soit au routeur du prochain saut, soit à l'adresse du dispositif de destination finale.

# Todo

3.1.12

3.3.6

3.6.6

3.7.11