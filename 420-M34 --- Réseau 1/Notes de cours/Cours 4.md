# Rôle de la couche physique

## Connexion physique

Le type de connexion physique utilisé dépend de la configuration du réseau. Par exemple, dans de nombreuses entreprises, les employés ont des ordinateurs portables ou de bureau qui sont connectés physiquement à un commutateur partagé par l'intermédiaire d'un câble. Ce type de configuration est appelé réseau filaire. Les données sont transmises par l'intermédiaire d'un câble physique.

Outre les connexions filaires, certaines entreprises proposent également des connexions sans fil pour les ordinateurs portables, les tablettes et les smartphones. Avec les périphériques sans fil, les données sont transmises par des ondes radio. Les connexions sans fil se développent à mesure que les particuliers et les entreprises découvrent les avantages de ce type de service. Les périphériques sur un réseau sans fil doivent être connectés à un point d'accès sans fil.

NICs

Les cartes d'interface réseau (NIC) permettent de connecter un périphérique au réseau. Les cartes réseau Ethernet sont utilisées dans les connexions filaires, tandis que les cartes réseau WLAN (réseau local sans fil) sont utilisées dans les connexions sans fil. Un périphérique utilisateur peut comporter l'un de ces deux types de carte réseau ou les deux. Une imprimante réseau, par exemple, peut ne comporter qu'une carte réseau Ethernet et doit, dans ce cas, être connectée au réseau à l'aide d'un câble Ethernet. D'autres périphériques, tels que les tablettes et les smartphones, peuvent n'être équipés que d'une carte réseau WLAN et doivent donc utiliser une connexion sans fil.

## Couche physique
La couche physique OSI fournit le moyen de transporter à travers le support réseau les bits constituant une trame de la couche liaison de données. Cette couche accepte une trame complète de la couche liaison de données et la code sous la forme d'une série de signaux transmis au support local. Les bits codés composant une trame sont reçus par un périphérique final ou intermédiaire.

La couche physique code les trames et crée les signaux électriques, optiques ou ondulatoires (radio) qui représentent les bits dans chaque trame. Ces signaux sont alors envoyés individuellement à travers le support.

La couche physique du noeud de destination récupère ces signaux individuels sur les supports, les convertit en représentations binaires et transmet les bits à la couche liaison de données sous forme de **trame complète**.

# Normes de la couche physique

Les protocoles et les opérations des couches OSI supérieures sont exécutés dans des logiciels conçus par des développeurs et des ingénieurs informaticiens. Les services et protocoles de la suite TCP/IP sont définis par l'Internet Engineering Task Force (IETF).

La couche physique est constituée de circuits électroniques, de supports et de connecteurs développés par des ingénieurs. Il est par conséquent approprié que les normes régissant ces matériels soient définies par les organisations d'ingénierie électrique et de communications correspondantes.

De nombreux organismes internationaux et nationaux, organisations gouvernementales de réglementation et entreprises privées sont impliqués dans l'établissement et la mise à jour des normes de couche physique. Par exemple, les normes relatives au matériel, aux supports, au codage et à la signalisation de la couche physique sont définies et régies par les organismes suivants:

* ISO (International Standards Organization).
* TIA/EIA (Telecommunications Industry Association/Electronic Industries Association)
* UIT (Union Internationale des Télécommunications)
* ANSI (American National Standards Institute)
* IEEE (Institute of Electrical and Electronics Engineers)
* Des autorités réglementaires nationales des télécommunications, notamment la Commission fédérale des communications (FCC) aux États-Unis et l'ETSI (European Telecommunications Standards Institute)
* Il existe également certains groupes régionaux de normalisation du câblage tels que la CSA (Canadian Standards Association), le CENELEC (European Committee for Electrotechnical Standardization) et le JSA/JIS (Japanese Standards Association) qui établissent des spécifications locales.

# Composants physiques
Les normes de couche physique couvrent trois domaines fonctionnels:

* Composants physiques 
* Codage
* Signalisation

Les composants physiques sont les périphériques électroniques, les supports et autres connecteurs qui transmettent les signaux qui représentent les bits. Les composants matériels, tels que les cartes réseau, les interfaces et les connecteurs, les matériaux et les conceptions de câble, sont tous spécifiés dans des normes associées à la couche physique. Les différents ports et interfaces d'un routeur Cisco 1941 sont également des exemples de composants physiques équipés de connecteurs et de brochages spécifiques définis dans des normes.

## Codage
Le codage, ou codage de ligne, est une méthode permettant de convertir un flux de bits de données en un « code » prédéfini. Les codes sont des groupements de bits utilisés pour fournir un modèle prévisible pouvant être reconnu à la fois par l'expéditeur et le récepteur. En d'autres termes, le codage est la méthode ou le modèle utilisé(e) pour représenter les informations numériques. Il est similaire au code Morse qui code un message en utilisant une série de points et de tirets.

Par exemple, le codage Manchester représente un bit 0 par une baisse de tension et un bit 1 par une hausse de tension. La transition a lieu au milieu de chaque période binaire. Ce type de codage est utilisé en 10 bit/s Ethernet. Les débits plus rapides requièrent un codage plus complexe. Le codage Manchester est utilisé dans les normes Ethernet les plus anciennes comme 10BASE-T. Ethernet 100BASE-TX utilise un codage 4B/5B et 1000BASE-T utilise un codage 8B/10B.

![](images/CodageManchester.png)

## Signalisation
La couche physique doit générer les signaux électriques, optiques ou sans fil qui représentent le 1 et le 0 sur le support. La méthode de représentation des bits est appelée méthode de signalisation. Les normes de couche physique doivent définir le type de signal représentant un 1 et celui représentant un 0. Il peut s'agir simplement d'un changement de niveau du signal électrique ou de l'impulsion optique. Par exemple, une impulsion longue peut représenter un 1, alors qu'une impulsion courte représente un 0.

Cela s'apparente à la méthode de signalisation utilisée dans le code Morse, qui utilise une série de tonalités, voyants ou clics pour envoyer du texte sur une ligne téléphonique ou entre des bateaux en mer.

## Bande passante

Différents supports physiques prennent en charge le transfert de bits à différents débits. Le transfert des données est généralement défini par la bande passante. La bande passante est la capacité d'un support à transporter des données. La bande passante numérique mesure la quantité d'informations pouvant circuler d'un emplacement à un autre pendant une période donnée. La bande passante est habituellement exprimée en kilobits par seconde (kbit/s), en mégabits par seconde (Mbit/s) ou en gigabits par seconde (Gbit/s). 

La bande passante est parfois considérée comme la vitesse à laquelle voyagent les bits, mais cette vision n'est pas exacte. Par exemple, en 10Mbit/s et en 100Mbit/s Ethernet, les bits sont envoyés à la vitesse de l'électricité. La différence correspond au nombre de bits transmis par seconde. Comme information supplémentaire, selon wikipedia, la vitesse de l'électricité est entre : 175 000 et 200 000 km/s.

Une combinaison de facteurs détermine la bande passante réelle d'un réseau:

* Les propriétés des supports physiques
* Les technologies choisies pour signaliser et détecter les signaux réseau
* Les propriétés des supports physiques, les technologies actuelles et les lois de la physique jouent toutes un rôle dans la détermination de la bande passante disponible.

Le tableau décrit les unités de mesure de la bande passante couramment utilisées:

Unité de bande passante	| Abréviation	| Équivalence
 --- | --- | ---
Bits par seconde | bits/s | 1 bit/s = unité fondamentale de bande passante
Kilobits par seconde |	Kbit/s	| 1 kbit/s = 1000 bit/s = 10^3 bit/s
Mégabits par seconde | Mbit/s	| 1 Mbit/s = 1000 000 bit/s = 10^6 bit/s
Gigabits par seconde | Gbits/s	| 1 Gbits/s = 1 000 000 000 bit/s = 10^9 bit/s
Térabits par seconde | Tbit/s	| 1 Tbit/s = 1 000 000 000 000 bits/s = 10^12 bits/s


## Définition
Les termes utilisés pour mesurer la qualité de la bande passante comprennent:

### Latence

La latence désigne le temps nécessaire (délais inclus) aux données pour voyager d'un point A à un point B.

Dans un interréseau ou un réseau à segments multiples, le débit ne peut pas être plus rapide que la liaison la plus lente du chemin menant de la source à la destination. Même si la totalité ou la plupart des segments ont une bande passante élevée, il suffit d'un segment dans le chemin de transmission avec un débit faible pour créer un goulot d'étranglement et ralentir le débit de l'ensemble du réseau.

### Débit

Le débit est la mesure du transfert de bits sur le support pendant une période donnée.

En raison d'un certain nombre de facteurs, le débit ne correspond généralement pas à la bande passante spécifiée dans les mises en oeuvre de couche physique. Le débit est généralement inférieur à la bande passante. trois facteurs qui ont une influence sur le débit:

* la quantité de trafic: dans le cas ou un lien réseau serait sur utilisé, il y aurait des chances que le paquet ne puisse se rendre à destination occasionnant une retransmission en TCP
* le type de trafic: dans le cas que de la prioritisation se fait à la destination, il est possible que si vos données ne sont pas prioritaires, elle pourrait être un peu trop longtemps avant d'être livrée.
* la latence: créée par le nombre de périphériques réseau rencontrés entre la source et la destination (distance)

Il existe de nombreux tests de débit en ligne qui peuvent indiquer le débit d'une connexion Internet: speedtest par exemple.

### Débit applicatif

Il existe une troisième mesure qui évalue le transfert de données utilisables, appelée débit applicatif. Le débit applicatif mesure les données utilisables transférées sur une période donnée. Le débit applicatif correspond donc au débit moins la surcharge de trafic pour l'établissement de sessions, les accusés de réception, l'encapsulation et les bits retransmis. Nommé *Goodput* en anglais, il est toujours inférieur au débit, qui est généralement inférieur à la bande passante.

# Caractéristiques du câblage en cuivre
Le câblage en cuivre est le type de câblage le plus courant utilisé dans les réseaux aujourd'hui. En fait, le câblage en cuivre n'est pas un seul type de câble. Il existe trois types différents de câblage en cuivre qui sont chacun utilisés dans des situations spécifiques.

Les supports en cuivre sont utilisés sur certains réseaux, car ils sont bon marché, faciles à installer et qu'ils présentent une faible résistance au courant électrique. Cependant, les supports en cuivre sont limités par la distance et les interférences du signal.

Les données sont transmises sur les câbles en cuivre sous forme d'impulsions électriques. Un détecteur dans l'interface réseau d'un périphérique de destination doit recevoir un signal pouvant être décodé correctement pour correspondre au signal envoyé. Mais, plus le signal voyage longtemps, plus il se détériore. C'est ce qu'on appelle l'atténuation du signal. Pour cette raison, tous les supports en cuivre sont soumis à des restrictions de distance strictes spécifiées par les normes de guidage.

La durée et la tension des impulsions électriques sont également susceptibles de subir des interférences de deux sources:

* Interférences électromagnétiques (EMI) ou interférences radioélectriques (RFI) - les signaux électromagnétiques et radioélectriques peuvent déformer et détériorer les signaux de données transportés par les supports en cuivre. Les sources potentielles d'interférences EMI et RFI sont notamment les ondes radio et les appareils électromagnétiques, tels que les éclairages fluorescents ou les moteurs électriques.
  
* Diaphonie - la diaphonie est la perturbation causée par les champs électriques ou magnétiques d'un signal dans un câble sur le signal traversant le câble adjacent. Dans les circuits téléphoniques, les interlocuteurs peuvent entendre une partie d'une autre conversation vocale provenant d'un circuit adjacent. Plus précisément, lorsqu'un courant électrique circule dans un câble, il crée un petit champ magnétique circulaire autour du câble qui peut être capté par le fil adjacent.

![](images/Interf%C3%A9rence.png)

Pour contrer les effets négatifs des perturbations électromagnétiques et radioélectriques, certains types de câbles en cuivre sont entourés d'un blindage métallique et nécessitent des connexions de mise à la terre appropriées.

Pour contrer les effets négatifs de la diaphonie, certains types de câbles en cuivre utilisent des paires de fils opposés torsadés qui annulent la perturbation.

La sensibilité des câbles en cuivre au bruit électronique peut également être limitée grâce à ces recommandations :

* en sélectionnant le type ou la catégorie de câble la mieux adaptée à un environnement réseau spécifique;
* en concevant une infrastructure de câblage évitant les sources d'interférences connues et potentielles dans la structure du bâtiment;
* en utilisant des techniques de câblage respectant les règles de manipulation et de terminaison des câbles.

Les types de câblage en cuivre
Trois types principaux de supports en cuivre sont utilisés dans les réseaux.

![](images/Blindage.png)

## Câble à paires torsadées non blindées (UTP)
Le câblage à paires torsadées non blindées (UTP) est le support réseau le plus répandu. Ces câbles UTP terminés par des connecteurs RJ-45 sont utilisés pour relier des hôtes réseau à des périphériques réseau intermédiaires, tels que des commutateurs et des routeurs.

Dans les réseaux locaux, chaque câble UTP se compose de quatre paires de fils à code couleur qui ont été torsadés, puis placés dans une gaine en plastique souple qui les protège des dégâts matériels mineurs. Le fait de torsader les fils permet de limiter les interférences causées par les signaux d'autres fils.

![](images/UTP.png)


## Câble à paires torsadées blindées (STP)

Les câbles à paires torsadées blindées (STP) offrent une meilleure protection contre les parasites que le câblage UTP. Ils sont toutefois bien plus onéreux et plus difficiles à installer que les câbles UTP. Comme les câbles UTP, les câbles STP utilisent un connecteur RJ-45.

Les câbles à paires torsadées blindées allient les techniques de blindage pour contrer les interférences électromagnétiques et radioélectriques, et les torsades pour éviter la diaphonie. Pour tirer entièrement parti des avantages du blindage, les câbles STP sont terminés par des connecteurs de données STP blindés spécifiques. Si le câble n'est pas correctement mis à la terre, le blindage peut agir comme une antenne et capter des signaux parasites.

Le câble STP représenté utilise quatre paires de fils, chacune enveloppée dans une feuille de blindage. Le tout est ensuite entouré dans une torsade ou une feuille métallique.

![](images/STP.png)

## Câble coaxial
Le câble coaxial (parfois abrégé en coax) tire son nom du fait qu'il contient deux conducteurs qui partagent le même axe. Comme l'illustre la figure, le câble coaxial est composé des éléments suivants:

Un conducteur en cuivre utilisé pour transmettre les signaux électroniques.
Une couche d'isolant plastique souple entoure un conducteur en cuivre.

Sur ce matériau isolant, une torsade de cuivre ou une feuille métallique constitue le second fil du circuit et fait office de protection pour le conducteur intérieur. Cette seconde couche, ou blindage, réduit également les interférences électromagnétiques externes.
Le câble dans son entier est ensuite entouré d'une gaine afin d'empêcher tout dégât matériel mineur.

Bien que les câbles UTP aient pratiquement remplacé les câbles coaxiaux dans les installations Ethernet modernes, la conception du câble coaxial est utilisée aux fins suivantes:

* Installations sans fil - les câbles coaxiaux relient les antennes aux périphériques sans fil. Le câble coaxial transporte de l'énergie en radiofréquence (RF) entre les antennes et l'équipement radio.
  
* Installations de câbles Internet - les fournisseurs de service par câble offrent une connexion Internet à leurs clients en remplaçant des sections du câble coaxial et en supportant des éléments d'amplification par du câble à fibre optique. Toutefois, le câblage à l'intérieur des locaux des clients reste coaxial.

![](images/RG6.png)

Différents types de connecteurs sont utilisés avec un câble coaxial. Les connecteurs Bayonet Neill—Concelman (BNC), de type N et de type F sont illustrés.

## Propriétés du câblage à paires torsadées non blindées

Lorsqu'il est utilisé comme support réseau, le câblage à paires torsadées non blindées (UTP) se compose de quatre paires de fils à code couleur en cuivre qui ont été torsadées, puis entourées d'une gaine en plastique souple. Son format compact peut être avantageux lors de l'installation.

Le câble UTP n'utilise pas de blindage pour contrer les effets des perturbations électromagnétiques et radioélectriques. En revanche, les concepteurs de câbles se sont rendu compte qu'il était possible de limiter les effets négatifs de la diaphonie:

Annulation- les concepteurs apparient désormais les fils du circuit. Lorsque deux fils d'un circuit électrique sont proches l'un de l'autre, leurs champs magnétiques sont l'exact opposé l'un de l'autre. Par conséquent, les deux champs magnétiques s'annulent et annulent également tous les signaux extérieurs d'EMI et de RFI.

Variation du nombre de torsades par paire de fils - pour renforcer l'effet d'annulation des paires de fils, les concepteurs utilisent un nombre différent de torsades pour chaque paire de fils d'un câble. Le câble UTP doit respecter des caractéristiques précises qui définissent le nombre de torsades autorisées par mètre (3,28 pieds) de câble. Dans la figure, vous observez que la paire orange/orange et blanc est moins torsadée que la paire bleu/bleu et blanc. Chaque couleur de paire présente un nombre de torsades différent.

![](images/Torsades.png)

Le câble UTP utilise uniquement l'effet d'annulation produit par les paires de fils torsadées pour limiter la dégradation du signal et pour protéger mutuellement les paires de fils des supports réseau.

## Normes de câblage et connecteurs UTP

Le câblage UTP respecte les normes établies conjointement par la Telecommunications Industry Association (TIA) et l'Electronic Industries Association (EIA). La norme TIA/EIA-568 en particulier, la plus souvent utilisée dans les environnements de câblage LAN, définit le câblage commercial pour les installations de réseau local. Certains des éléments définis sont les suivants :

* Types de câbles 
* Les longueurs de câbles 
* Connecteurs
* Terminaison du câble -Les méthodes de test des câbles

Les caractéristiques électriques du câblage en cuivre sont définies par l'IEEE (Institute of Electrical and Electronics Engineers). L'IEEE classe le câblage UTP suivant ses performances. Les câbles sont placés dans des catégories en fonction de leur capacité à prendre en charge des débits supérieurs de bande passante. Par exemple, un câble de catégorie 5 est généralement utilisé dans les installations Fast Ethernet 100BASE-TX. Les autres catégories comprennent le câble de catégorie 5 amélioré, la catégorie 6 et la catégorie 6a.

Les câbles des catégories supérieures sont conçus pour prendre en charge des débits de données plus élevés. À mesure que de nouvelles technologies Ethernet avec des débits exprimés en gigabits sont mises au point et adoptées, Cat5e constitue désormais le type de câble minimum acceptable, Cat6 étant recommandé pour les installations de nouveaux bâtiments.

La figure montre trois catégories de câbles UTP:

* La catégorie 3 a été utilisée à l'origine pour la communication vocale sur des lignes vocales, puis utilisée pour la transmission de données.
* Les catégories 5 et 5e sont utilisées pour la transmission des données. La catégorie 5 prend en charge 100 Mbps et la catégorie 5e prend en charge 1000 Mbps
* La catégorie 6 dispose d'un séparateur supplémentaire entre chaque paire de fils pour supporter des vitesses plus élevées. La catégorie 6 prend en charge jusqu'à 10 Gbit/s.
* La catégorie 7 prend également en charge 10 Gbps.
* La Catégorie 8 prend en charge 40 Gbps.

## Connecteurs UTP RJ-45

Le câble UTP se termine habituellement par un connecteur RJ-45. La norme TIA/EIA-568 décrit la correspondance des codes couleur des fils avec les broches (brochage) pour les câbles Ethernet.

La prise (ou le port) est le composant femelle d'un périphérique réseau, d'une prise murale ou fixée sur une cloison, ou d'un tableau de connexions. Mal raccordé, un câble constitue une source potentielle de dégradation des performances de la couche physique.

![](images/RJ45.png)

## Câble UTP mal raccordé

![](images/MauvaisCable.png)

## Câbles UTP droits et croisés

D'autres scénarios peuvent exiger des câbles UTP répondant à différentes conventions de câblage. Ceci signifie que les fils individuels du câble doivent être connectés dans des ordres différents à diverses séries de broches des connecteurs RJ-45.

Les principaux types de câbles obtenus en utilisant des conventions de câblage spécifiques sont les suivants :

* Câble Ethernet droit - type de câble réseau le plus courant. Il est généralement utilisé pour connecter un hôte à un commutateur et un commutateur à un routeur.
  
* Câble Ethernet croisé - câble peu utilisé permettant de relier des périphériques similaires. Par exemple, pour connecter un commutateur à un commutateur, un hôte à un autre hôte ou un routeur à un routeur. Cependant, les câbles croisés sont maintenant considérés comme hérités car les cartes réseau utilisent l'interface croisée dépendante du support (auto-MDIX) pour détecter automatiquement le type de câble et établir la connexion interne.

Note: Un autre type de câble est un câble rollover, qui est propriétaire de Cisco. Il est utilisé pour connecter un ordinateur à un routeur ou à un port de console de commutation.

**L'utilisation incorrecte d'un câble croisé ou droit entre des périphériques ne peut pas les endommager, mais la connectivité et la communication entre les périphériques deviennent alors impossibles.** Il s'agit d'une erreur courante en pratique et la vérification des connexions de périphériques doit constituer la première action de dépannage en cas de problème de connectivité.

Standard T568A et T568B

![](images/T568.png)

Type de câble | Standard | Application
---|---|---
Ethernet droit | T568A aux deux extrémités ou T568B aux deux extrémités	|Connecte un hôte réseau à un périphérique réseau tel qu'un commutateur ou un concentrateur.
Ethernet croisé | Une extrémité T568A, l'autre T568B | Connecte deux hôtes réseau, Connecte deux périphériques intermédiaires réseau (commutateur au commutateur ou routeur au routeur)
Inversé (Rollover) |	Exclusif à Cisco | Connecte un port série de station de travail à un port console de routeur, à l'aide d'un adaptateur.

# Câblage à fibre optique

## Propriétés de la fibre optique

Le câblage à fibre optique est l'autre type de câblage utilisé dans les réseaux. Parce qu'il est coûteux, il n'est pas aussi couramment utilisé dans les différents types de câblage en cuivre. Mais le câblage à fibre optique a certaines propriétés qui en font la meilleure option dans certaines situations, que vous découvrirez dans cette rubrique.

Les câbles à fibre optique transmettent les données sur de plus longues distances et avec une bande passante plus large que n'importe quel autre support réseau. Contrairement aux fils en cuivre, les câbles à fibre optique peuvent transmettre des signaux avec moins d'atténuation et sont entièrement protégés des perturbations électromagnétiques et radioélectriques. La fibre optique est couramment utilisée pour relier des périphériques réseau.

La fibre optique est un fil en verre très pur et transparent, à la fois flexible et très fin. Son diamètre n'est pas beaucoup plus grand que celui d'un cheveu humain. Les bits sont codés sur la fibre sous forme d'impulsions lumineuses. Le câble à fibre optique se comporte comme un guide d'ondes ou un « tuyau lumineux » pour transmettre la lumière entre les deux extrémités avec un minimum de perte de signal.

Pour mieux vous le représenter, imaginez un rouleau d'essuie-tout vide de plusieurs milliers de mètres de long, dont l'intérieur est recouvert d'un miroir. Un petit pointeur laser serait utilisé pour envoyer des signaux en code morse à la vitesse de la lumière. Un câble à fibre optique utilise le même principe, mais son diamètre est inférieur et utilise des technologies sophistiquées de signaux lumineux.

## Types de supports en fibre optique
Les câbles à fibre optique sont classés en deux types.

### Fibre optique monomode

La fibre monomode (SMF): se compose d'un très petit noyau et utilise une technologie laser coûteuse pour envoyer un seul rayon de lumière, comme le montre la figure. Elle est répandue dans les réseaux longue distance (plusieurs centaines de kilomètres), tels que ceux nécessaires pour les applications de téléphonie et de télévision par câble longue distance.

![](images/SMF.png))

### Fibre optique multimode

La fibre MMF se compose d'un noyau plus important et utilise des émetteurs à voyant (LED) moins coûteux pour envoyer des impulsions lumineuses. Plus précisément, la lumière d'une LED entre dans la fibre multimode sous différents angles. Elle est généralement utilisée dans les réseaux locaux, car elle permet l'utilisation de LED, dont le coût est faible. Elle fournit une bande passante allant jusqu'à 10 Gbit/s sur des liaisons pouvant atteindre 550 mètres de long.

![](images/MMF.png)


*Une différence notable entre la fibre multimode et la fibre monomode est le niveau de dispersion. La dispersion correspond à la propagation d'une impulsion lumineuse au fil du temps. Une dispersion accrue signifie une perte accrue de puissance du signal. La MMF a une plus grande dispersion que SMF. C'est pourquoi MMF ne peut voyager que jusqu'à 500 mètres avant la perte de signal.*

## Utilisation du câblage à fibre optique
Actuellement, les câbles à fibre optique sont utilisés dans quatre domaines d'application:

### Les réseaux d'entreprise

La fibre est utilisée pour les applications de câblage du réseau fédérateur et pour relier les périphériques d'infrastructure.

### La technologie FTTH (fiber to the home ou fibre optique jusqu'au domicile)

Cette technologie est utilisée pour fournir des services haut débit disponibles en permanence aux particuliers et aux petites entreprises.

### Les réseaux longue distance

Les câbles à fibre optique sont utilisés par les fournisseurs de services pour relier les pays et les villes.

### Les réseaux sous-marins

Des câbles spéciaux sont utilisés pour fournir des solutions haut débit et haute capacité fiables, capables de survivre dans des environnements sous-marins difficiles jusqu'à des distances transocéaniques.

https://www.submarinecablemap.com/

## Connecteurs à fibre optique

Un connecteur à fibre optique termine l'extrémité d'un câble à fibre optique. Divers connecteurs de ce type sont disponibles. Les principales différences entre les types de connecteurs sont les dimensions et les méthodes de couplage. Les entreprises décident en fonction de leur équipement des types de connecteurs qu'elles utiliseront.

Note: Certains commutateurs et routeurs ont des ports qui prennent en charge les connecteurs à fibre optique via un petit émetteur-récepteur SFP (small form-factor pluggable). 

Voir 4.5.4 pour les différents types de connecteurs. Ce n'est pas matière à l'examen.

*Jusqu'à récemment, la lumière ne pouvait circuler que dans une direction sur la fibre optique. Par conséquent, deux fibres sont nécessaires pour prendre en charge le mode duplex intégral. Par conséquent, les câbles de brassage en fibre optique regroupent deux câbles en fibre optique raccordés par une paire de connecteurs monovoies standard. Certains connecteurs à fibre optique acceptent à la fois les fibres de transmission et de réception. Ce sont des connecteurs duplex, comme les connecteurs LC duplex multimodes. Les normes BX telles que 100BASE-BX utilisent différentes longueurs d'onde pour l'envoi et la réception sur une seule fibre.*

## Cordons de brassage de fibre
Les câbles de brassage en fibre optique sont nécessaires pour interconnecter des périphériques d'infrastructure. 

L'utilisation de couleurs permet de différencier les câbles de brassage monomode et multimode. Une gaine jaune est utilisée pour les câbles à fibre optique monomodes et une gaine orange (ou aqua) pour les câbles multimodes.

# Fibre ou cuivre
Les câbles à fibre optique présentent de nombreux avantages par rapport aux câbles en cuivre. Le tableau souligne certaines de ces différences.

Actuellement, dans la plupart des environnements d'entreprise, la fibre optique est utilisée principalement comme câblage du réseau fédérateur pour les connexions point à point de trafic élevé entre des points de distribution de données. Il est également utilisé pour l'interconnexion de bâtiments dans des campus multi-bâtiments. La fibre optique ne conduisant pas l'électricité et subissant une perte de signal inférieure, elle est bien adaptée à ces usages.

Problèmes de mise en oeuvre | Câblage à paires torsadées non blindées (UTP)	|Câblage à fibre optique
--- |--- | --- | 
Bande passante	| 10 Mbit/s - 10 Gbit/s | 	10 Mbit/s - 100 Gbit/s
Distance | Relativement courte (1 à 100 mètres)	| Relativement longue (1 à 100 000 mètres)
Résistance aux perturbations électromagnétiques et radioélectriques	| Faible | Haute (résistance totale)
Résistance aux risques électriques	| Faible | Haute (résistance totale)
Coûts des supports et des connecteurs	| Moins élevé | Plus élevé
Compétences requises pour l'installation | Moins élevé | Plus élevé
Précautions à prendre concernant la sécurité | Moins élevé | Plus élevé

# Propriétés des supports sans fil

Les supports sans fil transportent à l'aide de fréquences radio et micro-ondes des signaux électromagnétiques qui représentent les chiffres binaires des communications de données.

Les supports sans fil offrent les meilleures options de mobilité, et le nombre d'appareils sans fil continue d'augmenter. La connexion sans fil est désormais la principale façon dont les utilisateurs se connectent aux réseaux domestiques et d'entreprise.

Voici quelques-unes des limites du sans-fil:

## La zone de couverture

les technologies de communication de données sans fil fonctionnent bien dans les environnements ouverts. Cependant, certains matériaux de construction utilisés dans les bâtiments et structures, ainsi que le terrain local, limitent la couverture effective.

## Les interférences 

la transmission sans fil est sensible aux interférences et peut être perturbée par des appareils aussi courants que les téléphones fixes sans fil, certains types d'éclairages fluorescents, les fours à micro-ondes et d'autres communications sans fil.

## La sécurité

la connexion à un réseau sans fil ne nécessite aucun accès physique à un support. Par conséquent, les périphériques et les utilisateurs non autorisés à accéder au réseau peuvent quand même accéder à la transmission. La sécurité du réseau est un composant essentiel de l'administration des réseaux sans fil.


## Le support partagé

les réseaux locaux sans fil fonctionnent en mode semi-duplex, ce qui signifie qu'un seul périphérique peut envoyer ou recevoir des données à la fois. Le support sans fil est partagé entre tous les utilisateurs sans fil. Plus le nombre d'utilisateurs ayant besoin d'accéder simultanément au réseau local sans fil est grand, moins il y a de bande passante disponible pour chacun d'entre eux.

Bien que la technologie sans fil soit de plus en plus utilisée pour la connectivité entre les ordinateurs de bureau, le cuivre et la fibre sont les supports de couche physique les plus populaires pour le déploiement de dispositifs de réseau intermédiaires, tels que les routeurs et les commutateurs.

## Types de supports sans fil

L'IEEE et les normes de l'industrie des télécommunications pour les communications de données sans fil couvrent à la fois les couches liaison de données et physique. Dans chacune de ces normes, des spécifications de couche physique sont appliquées à des domaines comprenant:

* le codage des données en signal radio ;
* la fréquence et la puissance de transmission ;
* les besoins relatifs à la réception et au décodage des signaux ;
* la conception et la mise en service des antennes.

Voici les normes sans fil:

### Wi-Fi (IEEE 802.11) 

La technologie sans fil LAN (WLAN) est généralement appelée Wi-Fi. Le WLAN utilise un protocole basé sur la contention connu sous le nom d'accès multiple par détection de porteuse et prévention des collisions (CSMA/CA). La carte réseau sans fil doit commencer par écouter avant d'émettre afin de déterminer si la canal radio est libre. 

Si un autre périphérique sans fil est en train d'émettre, la carte réseau doit alors attendre que le canal soit libre. Wi-Fi est une marque commerciale de la Wi-Fi Alliance. Le Wi-Fi est utilisé avec des appareils WLAN certifiés basés sur les normes IEEE 802.11.

### Bluetooth (IEEE 802.15) 

Il s'agit d'une norme de réseau personnel sans fil (WPAN), communément appelée «Bluetooth». Il utilise un processus d'appariement de dispositifs pour communiquer sur des distances allant de 1 à 100 mètres.

### WiMAX (IEEE 802:16) 

La technologie d'accès couramment appelée WiMAX (Worldwide Interoperability for Microwave Access) utilise une topologie point-à-multipoint pour fournir un accès à large bande sans fil.

### Zigbee (IEEE 802.15.4)

Zigbee est une spécification utilisée pour les communications à faible débit de données et de faible puissance. Il est destiné aux applications nécessitant une courte portée, des débits de données faibles et une longue durée de vie de la batterie. Zigbee est généralement utilisé pour les environnements industriels et Internet des objets (IoT) tels que les commutateurs lumineux sans fil et la collecte de données sur les dispositifs médicaux.

Note: D'autres technologies sans fil telles que les communications par satellite ou cellulaires peuvent également fournir une connectivité au réseau de données. 

## LAN sans fil

Une mise en oeuvre courante de réseau de données sans fil est de permettre à des périphériques de se connecter sans fil via un réseau local. Un réseau local sans fil exige généralement les périphériques réseau suivants:

### Point d'accès sans fil

concentre les signaux sans fil des utilisateurs et se connecte à l'infrastructure réseau en cuivre existante, telle qu'Ethernet. Comme illustré sur la figure, les routeurs sans fil domestiques et pour petite entreprise intègrent, dans un seul appareil, les fonctions d'un routeur, d'un commutateur et d'un point d'accès.

### Adaptateurs de carte réseau sans fil 
Ils offrent une capacité de communication sans fil aux hôtes du réseau.
Au fur et à mesure de la mise au point de cette technologie, un certain nombre de normes Ethernet WLAN ont émergé. 

L'acquisition de périphériques sans fil doit s'effectuer avec soin pour garantir la compatibilité et l'interopérabilité.

Les avantages des technologies de communication de données sans fil sont évidents, en particulier les économies sur le câblage coûteux des locaux et le côté pratique lié à la mobilité des hôtes. Les administrateurs réseau doivent mettre au point et appliquer des processus et des politiques de sécurité stricts pour protéger les réseaux locaux sans fil des accès non autorisés et des dégradations.

## Todo

4.1.3

4.2.7

4.3.6

4.4.4 (connaître l'ordre des couleurs n'est pas important pour l'examen, par contre, connaître la différence entre croisé et droit l'est)

4.5.7

4.6.4