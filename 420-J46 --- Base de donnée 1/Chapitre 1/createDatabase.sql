-- Michel Di Croci
-- Exemple 1
-- http://www.sqlite.org/sqlite.html
-- Pour plus d'informations

-- Création d'une base de données
-- .help

create table visiteurs (
vis_ID INTEGER Primary Key,
vis_nom varchar(50),
vis_prenom varchar(50),
vis_dateNaissance date,
vis_adresse varchar(50),
vis_ville varchar(50),
vis_codepostal varchar(7)
);


-- Insertion de données à l'intérieur de la table
insert into visiteurs values (NULL,1,1,1,1,1,1);

-- Exécution d'une recherche
select * from visiteurs;

-- Modification de l'affichage
.header on
.mode column
select * from visiteurs;

-- Autre mode d'affichage
.header off
.mode csv
select * from visiteurs;


-- Transformer la BD en ligne de commande SQL
.dump 
