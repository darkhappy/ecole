# -*- coding: utf-8 -*-

import urllib.request, codecs, sys
from bs4 import BeautifulSoup

def getWebsite(str):
    response = urllib.request.urlopen(str)
    html = response.read()
    soup = BeautifulSoup(html, 'html.parser')
    return soup

def find_href(soup):
    list_a = soup.find_all("a")
    for a in list_a:
            print(a)

def main():
    soup = getWebsite(sys.argv[1])
    # print(soup)
    find_href(soup)

if __name__ == "__main__":
    main()
