<?php

class home extends Controller
{
	public function index()
	{
		$user = $this->model('user');
		$user->setName("Bonjour");
		$this->view('home/index', ['name' => $user->getName()]);
	}

	public function submit()
	{

		$user = $this->model('user');
		$user->setName($_POST["name"]);
		$this->view('home/index', ['name' => $user->getName()]);
	}
}