var i = 0;

function timedCount() {
	if (i % 2 == 0) {
    	postMessage(i);
    }
	
	i = i + 1;
    timer = setTimeout("timedCount()", 200);
}



self.addEventListener('message', function(e) {
	var data = e.data;
	switch (data) {
	  case 'start':
		  timer = setTimeout("timedCount()", 200);
		  postMessage("Worker en exécution");
		break;
	  case 'pause':
		  clearTimeout(timer);		
		  postMessage("Worker en pause");
		break;
	};
  }, false);
