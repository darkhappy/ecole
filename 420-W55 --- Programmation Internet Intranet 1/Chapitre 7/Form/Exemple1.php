<?php

echo $_POST["txtNom"] . "<br/><br/>";
echo 'Navigation de la donnée $_POST: <br/>';

foreach ($_POST as $cle => $valeur)
	echo "$cle => $valeur<br/>";

/* 

L'utilisation de la variable hidden n'est pas une bonne idée car non seulement elle est facilement modifiable par le navigateur, elle est visible également en mode source dans le navigateur

*/
?>
<html>
<head>
<title>Bonjour les amis</title>
</head>
<body>

Information rentrée précédemment: <?=$_POST["txtNom"]?>

</body>
</html>
