<?php

$tab_donnees = array('bonjour', 'les', 'amis');
$tab_donnees = ['bonjour', 'les', 'amis'];

var_dump($tab_donnees);

echo "<br/>";

foreach ($tab_donnees as $donnees) {
    echo "$donnees<br/>";
}

/*
Il faut également noter que c'est un array indexé, i-e qu'il est accessible par l'entremise de $tab_donnees[0], $tab_donnees[1], $tab_donnees[2].
 */
