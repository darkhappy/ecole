
<?php
$foo = "0";  // $foo est une chaîne de caractères (ASCII 48)
echo '$foo = "0";<br/>' . "\n";
echo '$foo = ' . $foo . " type (" . gettype($foo) . ").<br/>\n";
$foo += 2;        // $foo est maintenant un entier (2)
echo '$foo += 2;<br/>\n';
echo '$foo = ' . $foo . " type (" . gettype($foo) . ").<br/>\n";
$foo = $foo + 1.3; // $foo est un nombre à virgule flottante (3.3)
echo '$foo + 1.3;<br/>\n';
echo '$foo = ' . $foo . " type (" . gettype($foo) . ").<br/>\n";
$foo = 5 + "3 petits cochons"; // $foo est un entier (8)
echo '$foo = 5 + 3 petits cochons;<br/>' . "\n";
echo '$foo = ' . $foo . " type (" . gettype($foo) . ").<br/>\n";
?>
