<?php 

	if (!file_exists("bd.sqlite3"))
	{
		try {
			$pdo = new PDO('sqlite:bd.sqlite3');
			
			// Créer la table au complet
			$pdo->exec("CREATE TABLE IF NOT EXISTS donnees (
			id INTEGER PRIMARY KEY, 
			nom TEXT, 
			prenom TEXT, 
			temps INTEGER)");
	
		} catch (PDOException $e) {
			echo 'Connection failed: ' . $e->getMessage();
		}
	}
	else
	{
		try {
			$pdo = new PDO('sqlite:bd.sqlite3');
		}
		catch(PDOException $ex){
    		die(json_encode(array('outcome' => false, 'message' => 'Unable to connect')));
    	}
	}

	function insertData($nom, $prenom)
	{
		global $pdo;


		$sql = "INSERT INTO donnees(nom, prenom, temps) VALUES (:nom, :prenom, :temps) ";
		$requete = $pdo->prepare($sql);

		$requete->bindValue(':nom', $nom);
		$requete->bindValue(':prenom', $prenom);
		$requete->bindValue(':temps', date(DATE_RFC2822));

		$requete->execute();
		return true;
	}

	function viewData()
	{
		global $pdo;

		$sql = "SELECT * FROM donnees";		
		$requete = $pdo->prepare($sql);
		$requete->execute();

		return $requete->fetchAll(PDO ::FETCH_ASSOCx);
	}

	


?>
