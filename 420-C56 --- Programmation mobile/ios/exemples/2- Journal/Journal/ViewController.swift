//
//  ViewController.swift
//  Journal
//
//  Created by Michel Di Croci on 2021-11-25.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    
    @IBOutlet weak var tableView: UITableView!
    var userEmail : String?
    var journals : [Journal]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let model = JournalModel()
        self.journals = model.getJournals()
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let journs = self.journals {
            return journs.count
        } else {
            return 0
        }
        
    }
        
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "Fancy",
                for: indexPath) as! mainTableViewCell
        
        if let journal = self.journals?[indexPath.row] {
            cell.name?.text = journal.name
            cell.subName?.text = journal.location
            if let defaultImage = UIImage(named: "upsala-glacier") {
                cell.coverImage?.image = defaultImage
            }
        }
        return cell
    }
    
}

