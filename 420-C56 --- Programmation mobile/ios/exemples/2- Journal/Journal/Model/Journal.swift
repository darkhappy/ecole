//
//  Journal.swift
//  Journal
//
//  Created by Michel Di Croci on 2021-11-25.
//

import Foundation

struct Journal {
    var key : String?
    var name : String?
    var location : String?
    var startDate : Date?
    var endDate : Date?
    var lat : Double?
    var lng : Double?
    var placeId : String?
    
    init(key: String?, name: String?, location: String?, startDate: Date?,
         endDate : Date?, lat: Double?, lng: Double?, placeId : String?)
    {
        self.key = key
        self.name = name
        self.location = location
        self.startDate = startDate
        self.endDate = endDate
        self.lat = lat
        self.lng = lng
        self.placeId = placeId
    }
    init(name: String?, location: String?, startDate: Date?, endDate : Date?,
         lat: Double?, lng: Double?, placeId : String?)
    {
        self.init(key: nil, name: name, location: location, startDate: startDate,
                  endDate: endDate, lat: lat, lng: lng, placeId: placeId)
    }
    init() {
        self.init(key: nil, name: nil, location: nil, startDate: nil, endDate: nil,
                  lat: nil, lng: nil, placeId: nil)
    }
}
