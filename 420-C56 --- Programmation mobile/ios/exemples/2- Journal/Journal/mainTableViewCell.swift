//
//  mainTableViewCell.swift
//  Journal
//
//  Created by Michel Di Croci on 2021-11-25.
//

import UIKit

class mainTableViewCell: UITableViewCell {

    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var subName: UILabel!
    @IBOutlet weak var coverImage: UIImageView!
    @IBOutlet weak var translucentView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.coverImage.backgroundColor = nil
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
