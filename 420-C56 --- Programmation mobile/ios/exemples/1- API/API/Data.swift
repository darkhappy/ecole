//
//  Data.swift
//  API
//
//  Created by Michel Di Croci on 2021-11-18.
//

import Foundation

struct jsonResponse: Codable {
    var data: DataResponse
    var support: supportResponse
}

struct DataResponse: Codable {
    var id: Int
    var email: String
    var first_name: String
    var last_name: String
    var avatar: String
}

struct supportResponse: Codable  {
    var url: String
    var text: String
}
