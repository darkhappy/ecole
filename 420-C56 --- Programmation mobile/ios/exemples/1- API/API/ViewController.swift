//
//  ViewController.swift
//  API
//
//  Created by Michel Di Croci on 2021-11-18.
//

import UIKit
import WebKit

class ViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let url = URL(string: "https://reqres.in/api/users/2")!
        let task = URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                self.handleClientError(error)
                return
            }
            guard let httpResponse = response as? HTTPURLResponse,
                (200...299).contains(httpResponse.statusCode) else {
                self.handleServerError(response)
                return
            }
            if let mimeType = httpResponse.mimeType, mimeType == "application/json", let data = data, let string = String(data: data, encoding: .utf8) {
                DispatchQueue.main.async {
                    let data = Data(string.utf8)
                    let user = try! JSONDecoder().decode(jsonResponse.self, from: data)
                    print(user.data.last_name)
                    
                    print(string)
                }
            }
        }
        task.resume()
        
    }

    func handleClientError( _ error: Error){
        print (error.localizedDescription)
    }
    
    func handleServerError( _ response: URLResponse?){
        print (response!)
    }
}

